﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NextGen.Framework.Integration.EBH.ServiceBusMessenger;
using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using Rhino.Mocks;
using System;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.Tests
{
    using Configuration;
    using Helper;
    using NextGen.Framework.Integration.EBH.DirectIntegration.Configuration;
    using ServiceBus;
	using ServiceBusMessenger.Queues;

	[TestClass]
	public class DirectIntegrationTests
	{
		IEBHIntegrationConfiguration config;
		ILogMgr logMgr;

		[TestInitialize]
		public void Setup()
        {
            var mockEbhWebServiceConfiguration = MockRepository.GenerateMock<IEbhWebServiceConfiguration>();
            mockEbhWebServiceConfiguration.Stub(h => h.RequestTimeout).Return("40000");
            mockEbhWebServiceConfiguration.Stub(h => h.Url).Return("https://test.net");
            mockEbhWebServiceConfiguration.Stub(h => h.ProviderKey).Return("some token");
            var mock = MockRepository.GenerateMock<IEBHIntegrationConfiguration>();
            mock.Stub(h => h.Mode).Return(IntegrationModes.Direct);
            mock.Stub(h => h.EbhWebServiceConfiguration).Return(mockEbhWebServiceConfiguration);
			config = mock;

            logMgr = MockRepository.GenerateMock<ILogMgr>();
			logMgr.Stub(x => x.ApplicationListeners).Return(new NxLogListenerCollection());
		}

		public IServiceBusMgr CreateService()
		{
			Globals.InitForOnPrem(logMgr);
			return EBHDirectIntegrationFactory.CreateInMemServiceBusMgr(config, logMgr);
		}

		#region Init

		[TestMethod, TestCategory("BDD")]
		public void Init_ConfigSection_IfConfigExist()
		{
			Assert.IsNotNull(config);
			Assert.IsNotNull(config.EbhWebServiceConfiguration.RequestTimeout);
			Assert.IsNotNull(config.EbhWebServiceConfiguration.Url);
			Assert.IsNotNull(config.EbhWebServiceConfiguration.ProviderKey);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(ArgumentNullException))]
		public void Init_ThrowsException_IfConfigSectionIsNull()
		{
			// Arrange
			IEBHIntegrationConfiguration noConfiguration = null;

			// Act
			EBHDirectIntegrationFactory.CreateInMemServiceBusMgr(noConfiguration, logMgr); // method should throw exception if first argument is Null
		}

		[TestMethod, TestCategory("TDD")]
		public void Init_Service_IsConnection()
		{
			// Arrange, Act
			var service = CreateService();

			// Assert
			Assert.IsNotNull(service);
			Assert.IsTrue(service.IsConnected);
		}

		#endregion

		#region Service

		[TestMethod, TestCategory("TDD")]
		public void Service_RegisteredQueues_AreNotNull()
		{
			// Arrange, Act
			var service = CreateService();

			// Assert
			Assert.IsNotNull(service);
            Assert.IsNotNull(service.RegisteredQueues);
			Assert.AreEqual(11, service.RegisteredQueues.Count);
		}

		[TestMethod, TestCategory("TDD")]
		public void Service_Namespace_IsNotNull()
		{
			// Arrange, Act
			var service = CreateService();

			// Assert
			Assert.IsNotNull(service);
			Assert.IsNotNull(service.GetNamespace(QueueNames.Namespace));
		}

		[TestMethod, TestCategory("TDD")]
		public void Service_Namespace_IsNotAzure()
		{
			// Arrange, Act
			var service = CreateService();

			// Assert
			Assert.IsNotNull(service);
			var namespaceMgr = service.GetNamespace(QueueNames.Namespace);
			Assert.IsNotNull(namespaceMgr);
			Assert.IsFalse(namespaceMgr.IsAzure);
		}

		[TestMethod, TestCategory("TDD")]
		public void Service_Namespace_HasName()
		{
			// Arrange, Act
			var service = CreateService();

			// Assert
			Assert.IsNotNull(service);
			Assert.AreEqual(service.GetNamespace(QueueNames.Namespace).Name, QueueNames.Namespace);
			Assert.AreEqual(service.GetNamespace(NamespaceTarget.EBH).Name, nameof(NamespaceTarget.EBH));
		}

		[TestMethod, TestCategory("TDD")]
		public void Service_TryCompleteMsg_True()
		{
			// Arrange, Act
			var service = CreateService();

			// Assert
			Assert.IsNotNull(service);
			Assert.IsTrue(service.TryCompleteMsg(new InMemQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, null), null, 10));
			Assert.IsTrue(service.TryCompleteMsg(new InMemMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, null), null, 10));
		}

		[TestMethod, TestCategory("TDD")]
		public void Service_QueueClient_IsNotNull()
		{
			// Arrange, Act
			var service = CreateService();

			// Assert
			Assert.IsNotNull(service);
			Assert.IsNotNull(service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest));
		}

		[TestMethod, TestCategory("TDD")]
		public void Service_MessageSession_IsNotNull()
		{
			// Arrange, Act
			var service = CreateService();

			// Assert
			Assert.IsNotNull(service);
			Assert.IsNotNull(service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString()));
		}

		[TestMethod, TestCategory("TDD")]
		public void Service_MessageSession_IsClosed()
		{
			// Arrange
			var service = CreateService();

			// Act
			var messageSession = service?.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString());

			// Assert
			Assert.IsNotNull(service);
			Assert.IsNotNull(messageSession);
			Assert.IsFalse(messageSession.IsClosed);
		}

		#endregion

		#region QueueClient

		[TestMethod, TestCategory("TDD")]
		public void QueueClient_SendReceiveMessage_EBHVALIDATERESPONSE()
		{
			// Arrange
			var service = CreateService();
			var sessionId = Guid.NewGuid().ToString();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhValidateResponse);
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhValidateResponse, sessionId);
			var message = GetMessage(sessionId, "lb", "key", "value");

			// Act
			queueClient.Send(message);
			var response1 = messageSession.Receive();
			var response2 = messageSession.Receive(); // message should be removed after receive, if not probably will be issue with memory leak

			// Assert
			Assert.IsNotNull(response1);
			Assert.IsNull(response2);
			Assert.AreEqual(message.Label, response1.Label);
			Assert.AreEqual(message.Properties["key"], response1.Properties["key"]);
			Assert.AreEqual(message.SessionId, response1.SessionId);
		}

		[TestMethod, TestCategory("TDD")]
		public void QueueClient_SendTwoMessagseInOneSession_EBHVALIDATERESPONSE()
		{
			// Arrange
			var service = CreateService();
			var sessionId = Guid.NewGuid().ToString();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhValidateResponse);
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhValidateResponse, sessionId);
			var message1 = GetMessage(sessionId, "lb1", "key1", "value1");
			var message2 = GetMessage(sessionId, "lb2", "key2", "value2");

			// Act
			queueClient.Send(message1);
			queueClient.Send(message2);
			var response = messageSession.Receive();

			// Assert
			Assert.IsNotNull(response);
			Assert.AreEqual(message2.Label, response.Label);
			Assert.IsFalse(response.Properties.ContainsKey("key1"));
			Assert.AreEqual(message2.Properties["key2"], response.Properties["key2"]);
			Assert.AreEqual(message2.SessionId, response.SessionId);
		}

		[TestMethod, TestCategory("TDD")]
		public void QueueClient_SendReceiveMessage_EBHVALIDATEREQUEST()
		{
			// Arrange
			var service = CreateService();
			var sessionId = Guid.NewGuid().ToString();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhValidateRequest);
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhValidateResponse, sessionId);
			var key = "key1";
			var message = GetMessage(sessionId, "lb1", key, "value1");

			// Act
			queueClient.Send(message);
			var response = messageSession.Receive();

			// Assert
			Assert.IsNotNull(response);
			Assert.AreEqual(message.Label, response.Label);
			Assert.AreEqual(message.Properties[key], response.Properties[key]);
			Assert.AreEqual(message.SessionId, response.SessionId);
		}


		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_SendMessageOnNotExistingQueue()
		{
			// Arrange
			var service = CreateService();
			var sessionId = Guid.NewGuid().ToString();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, "test");
			var message = GetMessage(sessionId, "lb", "key", "value");

			// Act
			queueClient.Send(message); // method should throw exception if queue is not exist
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(ArgumentNullException))]
		public void QueueClient_SendMessage_IfMessageIsNull()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, "test");

			// Act
			queueClient.Send(null); // method should throw exception if Argument is Null
		}

		#endregion

		#region MessageSession

		[TestMethod, TestCategory("TDD")]
		public void When_MessageSession_Close_MessagesCleared()
		{
			// Arrange
            var messageRouter = new MessageRouter();
            var sessionId = Guid.NewGuid().ToString();

            var brokeredMessage = GetMessage(sessionId, "lbl", "key", "value");

            messageRouter.RegisterMessageHandler(
				QueueNames.EbhInvoiceStatusRequest,
                (message) => messageRouter.ProcessSend(QueueNames.EbhInvoiceStatusResponse, brokeredMessage));
            var sbMgr = new InMemServiceBusMgr(messageRouter);
            var queueReq = sbMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

            queueReq.Send(brokeredMessage);
            var messageSession = sbMgr.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusResponse, sessionId);

			// Act
			var msg1 = messageSession.Receive();
			messageSession.Close(); // method should clear messages
            var msg2 = messageSession.Receive();

			// Assert
			Assert.IsNotNull(msg1);
			Assert.IsNull(msg2);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void MessageSession_ThrowsException_ReceiveByTimeSpan()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhValidateRequest, Guid.NewGuid().ToString());

			// Act
			var message = messageSession.Receive(new TimeSpan());
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void MessageSession_ThrowsException_Receive()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhValidateRequest, Guid.NewGuid().ToString());

			// Act
			var message = messageSession.Receive();
		}

		[TestMethod, TestCategory("TDD")]
		public void MessageSession_ReceiveMessageByTimeSpan_IfMessageNotExist()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhValidateResponse, Guid.NewGuid().ToString());

			// Act
			var message = messageSession.Receive(new TimeSpan());

			// Assert
			Assert.IsNull(message);
		}

		[TestMethod, TestCategory("TDD")]
		public void MessageSession_ReceiveMessage_IfMessageNotExist()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhValidateResponse, Guid.NewGuid().ToString());
			var message = messageSession.Receive(new TimeSpan());

			// Act
			var message2 = messageSession.Receive();

			// Assert
			Assert.IsNull(message);
			Assert.IsNull(message2);
		}

		#endregion

		#region Service_NotSupportedException

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void Service_ThrowsException_PurgeQueue()
		{
			// Arrange
			var service = CreateService();

			// Act
			service.PurgeQueue(QueueNames.Namespace, QueueNames.EbhConfigurationRequest);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void Service_ThrowsException_PurgeQueues()
		{
			// Arrange
			var service = CreateService();

			// Act
			service.PurgeQueues();
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void Service_ThrowsException_RegisterNamespace()
		{
			// Arrange
			var service = CreateService();

			// Act
			service.RegisterNamespace(QueueNames.Namespace, NamespaceTarget.EBH);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void Service_ThrowsException_RegisterNamespaceAzure()
		{
			// Arrange
			var service = CreateService();

			// Act
			service.RegisterNamespaceAzure(QueueNames.Namespace, NamespaceTarget.EBH);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void Service_ThrowsException_RegisterQueue()
		{
			// Arrange
			var service = CreateService();

			// Act
			service.RegisterQueue(QueueNames.Namespace, QueueNames.EbhConfigurationRequest, false);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void Service_ThrowsException_RegisterQueueAzure()
		{
			// Arrange
			var service = CreateService();

			// Act
			service.RegisterQueueAzure(QueueNames.Namespace, QueueNames.EbhConfigurationRequest, string.Empty, false);
		}

		#endregion

		#region Namespace_NotSupportedException

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void Namespace_ThrowsException_PurgeQueue()
		{
			// Arrange
			var service = CreateService();
			var ns = service.GetNamespace(QueueNames.Namespace);

			// Act
			ns.CreateQueue(QueueNames.EbhConfigurationRequest, new TimeSpan(), 10, true);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void Namespace_ThrowsException_DeleteQueue()
		{
			// Arrange
			var service = CreateService();
			var ns = service.GetNamespace(QueueNames.Namespace);

			// Act
			ns.DeleteQueue(QueueNames.EbhConfigurationRequest);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void Namespace_ThrowsException_QueueExists()
		{
			// Arrange
			var service = CreateService();
			var ns = service.GetNamespace(QueueNames.Namespace);

			// Act
			ns.QueueExists(string.Empty);
		}

		#endregion

		#region QueueClient_NotSupportedException

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_CreateJsonMessage()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.CreateJsonMessage(null);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_AcceptMessageSession()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.AcceptMessageSession(string.Empty);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_OnMessageAsync()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.OnMessageAsync(null);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_OnMessage()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.OnMessage(null, 1);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_OnMessageCallback()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.OnMessage(null);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_ReceiveAsyncBySequenceNumber()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.ReceiveAsync(1);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_ReceiveAsyncByTimeSpan()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.ReceiveAsync(new TimeSpan());
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_ReceiveAsync()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.ReceiveAsync();
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_ReceiveBySequenceNumber()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.Receive(1);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_ReceiveByTimeSpan()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.Receive(new TimeSpan());
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void QueueClient_ThrowsException_Receive()
		{
			// Arrange
			var service = CreateService();
			var queueClient = service.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);

			// Act
			queueClient.Receive();
		}

		#endregion

		#region MessageSession_NotSupportedException

		[TestMethod, TestCategory("TDD")]
		public void GetMessageSession_SessionObjIsDisposed()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString());

			// Act
			messageSession.Dispose();

			// Assert
			Assert.IsTrue(messageSession.IsClosed);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void MessageSession_ThrowsException_OnMessageAsync()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString());

			// Act
			messageSession.OnMessageAsync(null);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void MessageSession_ThrowsException_OnMessage()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString());

			// Act
			messageSession.OnMessage(null);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void MessageSession_ThrowsException_OnMessageWithConcurrentCalls()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString());

			// Act
			messageSession.OnMessage(null, 1);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void MessageSession_ThrowsException_ReceiveAsyncBySequenceNumber()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString());

			// Act
			messageSession.ReceiveAsync(1);
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void MessageSession_ThrowsException_ReceiveAsyncTimeSpan()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString());

			// Act
			messageSession.ReceiveAsync(new TimeSpan());
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void MessageSession_ThrowsException_ReceiveAsync()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString());

			// Act
			messageSession.ReceiveAsync();
		}

		[TestMethod, TestCategory("TDD")]
		[ExpectedException(typeof(NotSupportedException))]
		public void MessageSession_ThrowsException_ReceiveBySequenceNumber()
		{
			// Arrange
			var service = CreateService();
			var messageSession = service.GetMessageSession(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest, Guid.NewGuid().ToString());

			// Act
			messageSession.Receive(1);
		}

		#endregion

		private IBrokeredMessage GetMessage(string sessionId, string label, string key, string value)
		{
			var mess = new InMemBrokeredMessage() 
			{ 
				CorrelationId = Guid.NewGuid().ToString(),
				Label = label, 
				MessageId = Guid.NewGuid().ToString(),
				SessionId = sessionId, 
				TimeToLive = new TimeSpan(0, 1, 0),  
			};
			mess.Properties.Add(key, value);

			return mess;
		}
	}
}
