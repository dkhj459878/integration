﻿using Microsoft.Azure.WebJobs.Host;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace NextGen.Framework.Integration.EBH.ServiceBusFunctions
{
    public class LogBucket : ILogger
    {
        IList<string> messages = new List<string>();

        public IDisposable BeginScope<TState>(TState state)
        {
            return NullLogger.Instance.BeginScope(state);
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
           messages.Add(state.ToString());
        }

        public void SyncTo(ILogger realLogger)
        {
            foreach (var item in messages)
            {
                realLogger.LogInformation(item);
            }

            messages.Clear();
        }
    }
}