using Microsoft.Azure.WebJobs;
using NextGen.Framework.Managers.LogMgr;
using System;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs.ServiceBus;
using Microsoft.Extensions.Logging;
using NextGen.Framework.Integration.EBH.ServiceBusMessenger;


namespace _3E.Func
{
    using NextGen.Framework.Integration.EBH.Helper;
    using NextGen.Framework.Integration.EBH.ServiceBusMessenger.Messengers;
    using NextGen.Framework.Integration.EBH.ServiceBusMessenger.Queues;
    using NextGen.Framework.Integration.EBH.ServiceBusFunctions;

    public static class Functions
    {
        private static LogBucket preInitLogger = new LogBucket();
        private static ConfigValuesProviderAzure configValuesProviderAzure;
        private static object locker = new object();

        [FunctionName(nameof(getEBHValidateRequest))]
        public static void getEBHValidateRequest(
            [ServiceBusTrigger("%ebhvalidaterequestqn%", Connection = "SB" + nameof(Queues.EBHVALIDATEREQUEST))] Message myQueueItem,
            [ServiceBus("%ebhvalidatependingqn%", Connection = "SB" + nameof(Queues.EBHVALIDATEPENDING))] ICollector<Message> collectorPending, IMessageSession messageSession,
            ILogger log)
        {
            var logr = new AzLogMgr(log);
            var result = Init(log, logr);
            var azMessage = new NxAzBrokeredMessage(myQueueItem, messageSession);
            var serviceBusHandler = new NxAzServiceBusHandler(collectorPending);

            var validateReceiver = new EbhServiceBusValidationReceiver(serviceBusHandler, ResolveWebSvcHandler(logr), null, logr);

            if (result.IsSuccess)
            {
                validateReceiver.ProcessMessagePump(azMessage);
            }
            else
            {
                validateReceiver.SendResponseErrorMsg(azMessage, result.ErrorMessage);
            }
        }

        [FunctionName(nameof(putEBHValidatePending))]
        [return: ServiceBus("%ebhvalidatependingqn%", Connection = "SB" + nameof(Queues.EBHVALIDATEPENDING), EntityType = EntityType.Queue)]
        public static Message putEBHValidatePending([HttpTrigger] Message myQueueItem, ILogger log)
        {
            return myQueueItem;
        }

        [FunctionName(nameof(getEBHValidatePending))]
        public static void getEBHValidatePending(
            [ServiceBusTrigger("%ebhvalidatependingqn%", Connection = "SB" + nameof(Queues.EBHVALIDATEPENDING))] Message myQueueItem, IMessageSession messageSession,
            [ServiceBus("%ebhvalidateresponseqn%")] ICollector<Message> collectorResponse,
            ILogger log)
        {
            var logr = new AzLogMgr(log);
            var result = Init(log, logr);
            var azMessage = new NxAzBrokeredMessage(myQueueItem, messageSession);

            var serviceBusHandler = new NxAzServiceBusHandler(collectorResponse);
            var validateSender = new EbhServiceBusValidationResultReciever(serviceBusHandler, ResolveWebSvcHandler(logr), null, logr);

            if (result.IsSuccess)
            {
                validateSender.ProcessMessagePump(azMessage);
            }
            else
            {
                validateSender.SendResponseErrorMsg(azMessage, result.ErrorMessage);
            }
        }

        [FunctionName(nameof(putEBHValidateResponse))]
        [return: ServiceBus("%ebhvalidateresponseqn%")]
        public static Message putEBHValidateResponse([HttpTrigger] Message myQueueItem, ILogger log)
        {
            return myQueueItem;
        }

        [FunctionName(nameof(getEBHConfigurationRequest))]
        public static void getEBHConfigurationRequest(
            [ServiceBusTrigger("%ebhconfigurationrequestqn%", Connection = "SB" + nameof(Queues.EBHCONFIGURATIONREQUEST))] Message myQueueItem, IMessageSession messageSession,
            [ServiceBus("%ebhconfigurationresponseqn%")] ICollector<Message> collectorResponse,
            ILogger log)
        {
            var logr = new AzLogMgr(log);
            var result = Init(log, logr);
            var azMessage = new NxAzBrokeredMessage(myQueueItem, messageSession);
            var serviceBusHandler = new NxAzServiceBusHandler(collectorResponse);
            var configReceiver = new EbhServiceBusConfigurationReceiver(serviceBusHandler, ResolveWebSvcHandler(logr), null, logr);

            if (result.IsSuccess)
            {

                configReceiver.ProcessMessagePump(azMessage);
            }
            else
            {
                configReceiver.SendResponseErrorMsg(azMessage, result.ErrorMessage);
            }
        }

        [FunctionName(nameof(putEBHConfigurationResponse))]
        [return: ServiceBus("%ebhconfigurationresponseqn%")]
        public static Message putEBHConfigurationResponse([HttpTrigger] Message myQueueItem, ILogger log)
        {
            return myQueueItem;
        }

        [FunctionName(nameof(getEBHInvSubRequest))]
        public static void getEBHInvSubRequest(
            [ServiceBusTrigger("%ebhinvsubrequestqn%", Connection = "SB" + nameof(Queues.EBHINVSUBREQUEST))] Message myQueueItem, IMessageSession messageSession,
            [ServiceBus("%ebhinvsubresponseqn%")] ICollector<Message> collector,
            ILogger log)
        {
            
            var logr = new AzLogMgr(log);
            var result = Init(log, logr);
            var azMessage = new NxAzBrokeredMessage(myQueueItem, messageSession);
            var serviceBusHandler = new NxAzServiceBusHandler(collector);

            var invSubReceiver = new EbhServiceBusInvoiceSubmissionReceiver(serviceBusHandler, ResolveWebSvcHandler(logr), null, logr);

            if (result.IsSuccess)
            {
                invSubReceiver.ProcessMessagePump(azMessage);
            }
            else
            {
                invSubReceiver.SendResponseErrorMsg(azMessage, result.ErrorMessage);
            }
        }

        [FunctionName(nameof(putEBHInvSubResponse))]
        [return: ServiceBus("%ebhinvsubresponseqn%")]
        public static Message putEBHInvSubResponse([HttpTrigger] Message myQueueItem, ILogger log)
        {
            return myQueueItem;
        }

        [FunctionName(nameof(getEBHInvStatRequest))]
        public static void getEBHInvStatRequest(
            [ServiceBusTrigger("%ebhinvstatrequestqn%", Connection = "SB" + nameof(Queues.EBHINVSTATREQUEST))] Message myQueueItem, IMessageSession messageSession,
            [ServiceBus("%ebhinvstatresponseqn%")] ICollector<Message> collector,
            ILogger log)
        {
            var logr = new AzLogMgr(log);
            var result = Init(log, logr);
            var azMessage = new NxAzBrokeredMessage(myQueueItem, messageSession);

            var serviceBusHandler = new NxAzServiceBusHandler(collector);
            var invStatReceiver = new EbhServiceBusInvoiceStatusReceiver(serviceBusHandler, ResolveWebSvcHandler(logr), null, logr);

            if (result.IsSuccess)
            {
                invStatReceiver.ProcessMessagePump(azMessage);
            }
            else
            {
                invStatReceiver.SendResponseErrorMsg(azMessage, result.ErrorMessage);
            }
        }

        [FunctionName(nameof(putEBHInvStatResponse))]
        [return: ServiceBus("%ebhinvstatresponseqn%")]
        public static Message putEBHInvStatResponse([HttpTrigger] Message myQueueItem, ILogger log)
        {
            return myQueueItem;
        }

        [FunctionName(nameof(getEBHAttachmentRequest))]
        public static void getEBHAttachmentRequest(
            [ServiceBusTrigger("%ebhattachmentrequestqn%", Connection = "SB" + nameof(Queues.EBHATTACHMENTREQUEST))] Message myQueueItem, IMessageSession messageSession,
            [ServiceBus("%ebhattachmentresponseqn%")] ICollector<Message> collector,
            ILogger log)
        {
            var logr = new AzLogMgr(log);
            var result = Init(log, logr);
            var azMessage = new NxAzBrokeredMessage(myQueueItem, messageSession);
            var serviceBusHandler = new NxAzServiceBusHandler(collector);

            var receiver = new EbhServiceBusAttachmentReceiver(serviceBusHandler, ResolveWebSvcHandler(logr), null, logr);

            if (result.IsSuccess)
            {
                receiver.ProcessMessagePump(azMessage);
            }
            else
            {
                receiver.SendResponseErrorMsg(azMessage, result.ErrorMessage);
            }
        }

        [FunctionName(nameof(putEBHAttachmentResponse))]
        [return: ServiceBus("%ebhattachmentresponseqn%")]
        public static Message putEBHAttachmentResponse([HttpTrigger] Message myQueueItem, ILogger log)
        {
            return myQueueItem;
        }

        private static InitResult Init(ILogger log, ILogMgr logMgr)
        {
            InitResult result = new InitResult(null);
            lock (locker)
            {
                try
                {

                    if (configValuesProviderAzure == null)
                    {
                        configValuesProviderAzure = new ConfigValuesProviderAzure(preInitLogger);
                    }

                    Globals.InitForAzureFunc(configValuesProviderAzure);
                    preInitLogger.SyncTo(log);

                    Globals.InitForAzureLog(logMgr);
                }
                catch (Exception e)
                {
                    logMgr.Error(e);
                    result = new InitResult(e);
                }
            }
            return result;
        }

        private static EbhWebService ResolveWebSvcHandler(ILogMgr log) => new EbhWebService(new EbhWebServiceConfiguration(), log);

        private class InitResult
        {
            private readonly Exception _exception;

            public InitResult(Exception ex)
            {
                _exception = ex;
            }

            public bool IsSuccess => _exception == null;
            public string ErrorMessage => _exception.Message;
        }
    }
}
