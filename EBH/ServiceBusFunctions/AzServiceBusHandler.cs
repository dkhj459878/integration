﻿using System;
using System.Collections.Generic;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using NextGen.Framework.Managers.ServiceBusMgr;
using IQueueClient = NextGen.Framework.Managers.ServiceBusMgr.IQueueClient;

namespace _3E.Func
{
    public class NxAzServiceBusHandler : IServiceBusHandler
    {

        private const int Deftimetolive = 20;
        private readonly ICollector<Message> _currentCollector;


        public NxAzServiceBusHandler(ICollector<Message> msgCollector)
        {
            _currentCollector = msgCollector;

        }
        public bool IsConnected
        {
            get
            {
                //can't do put test
                return true;
                //try
                //{
                //    eBHSBFuncs.putEBHConfigurationResponse(new IBrokeredMessage() { TimeToLive = new TimeSpan(0, 0, 2) });
                //}
                //catch
                //{ return false; }
                //return true;
            }
        }

        public IBrokeredMessage CreateBrokeredMessage()
        {
            return new NxAzBrokeredMessage { TimeToLive = new TimeSpan(0, Deftimetolive, 0) };
        }

        public IBrokeredMessage CreateBrokeredMessage(IBrokeredMessage originalMsg, IDictionary<string, object> extraProps = null)
        {
            return new NxAzBrokeredMessage(originalMsg, extraProps) { TimeToLive = new TimeSpan(0, Deftimetolive, 0) };
        }

        public bool TryCompleteMsg(IBrokeredMessage msg)
        {
            try
            {
                msg.Complete();
            }
            catch { return false; }
            return true;
        }

        public void SendMessage(IBrokeredMessage msg)
        {
            _currentCollector.Add(msg.ToNativeMessage() as Message);
        }

        public IQueueClient ReceiveMessage(Action<IBrokeredMessage> messageHandler, int maxConcurrentCalls)
        {
            throw new NotSupportedException("Not supported for azure implementation.");
        }
    }

    public class NxAzBrokeredMessage : IBrokeredMessage
    {
        internal Message Ms = new Message();

        private readonly Microsoft.Azure.ServiceBus.Core.IMessageReceiver messageReceiver;

        private readonly IDictionary<string, object> extraProperties;

        private const string StatusCode = "statuscode";

        public NxAzBrokeredMessage()
            : this(NewMessageId())
        {
        }


        public NxAzBrokeredMessage(IBrokeredMessage originalMessage, IDictionary<string, object> extraProps)
            : this(NewMessageId())
        {
            if (originalMessage != null)
                CopyMsgProps(originalMessage);

            if (extraProps == null) return;

            extraProperties = extraProps;
            SetExtraProps();
        }

        public NxAzBrokeredMessage(Message ms, Microsoft.Azure.ServiceBus.Core.IMessageReceiver messageReceiver)
            : this(NewMessageId())
        {
            Ms = ms;
            this.messageReceiver = messageReceiver;
        }

        private NxAzBrokeredMessage(string newMessageId)
        {
            MessageId = newMessageId;
        }

        private static string NewMessageId()
        {
            return Guid.NewGuid().ToString("N");
        }

        public string CorrelationId
        {
            get => Ms.CorrelationId;

            set => Ms.CorrelationId = value;
        }

        public string Label
        {
            get => Ms.Label;

            set => Ms.Label = value;
        }

        public string MessageId
        {
            get => Ms.MessageId;

            set => Ms.MessageId = value;
        }

        public IDictionary<string, object> Properties => Ms.UserProperties;

        public long SequenceNumber => Ms.Size;

        public string SessionId
        {
            get => Ms.SessionId;

            set => Ms.SessionId = value;
        }

        public TimeSpan TimeToLive
        {
            get => Ms.TimeToLive;

            set => Ms.TimeToLive = value;
        }

        private void CopyMsgProps(IBrokeredMessage source)
        {
            CorrelationId = source.CorrelationId;
            Label = source.Label;
            MessageId = source.MessageId;
            SessionId = source.SessionId;
            foreach (var kv in source.Properties)
                Properties.Add(kv.Key, kv.Value);
        }

        private void SetExtraProps()
        {
            if (extraProperties == null)
                return;

            foreach (var property in extraProperties)
            {
                if (!Properties.ContainsKey(property.Key))
                    AddExtraProperty(property);
            }
        }

        private void AddExtraProperty(KeyValuePair<string, object> property)
        {
            var value = property.Key == StatusCode ? property.Value.ToString() : property.Value;

            if (!Properties.ContainsKey(property.Key))
                Properties.Add(property.Key, value);
        }

        public void Complete()
        {
            if (messageReceiver != null)
            {
                Ms.Complete(messageReceiver);
            }
        }

        public virtual bool RenewLock()
        {
            try
            {
                if (messageReceiver != null)
                {
                    Ms.RenewLock(messageReceiver);
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
            return true;
        }

        public string ReplyTo
        {
            get => Ms.ReplyTo;
            set => Ms.ReplyTo = value;
        }

        public object ToNativeMessage()
        {
            return Ms;
        }
    }
}
