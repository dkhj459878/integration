﻿using System;
using Microsoft.Extensions.Logging;
using NextGen.Framework.Managers.LogMgr;

namespace _3E.Func
{
    public class AzLogMgr : ILogMgr
    {
        private ILogger _log;
        public AzLogMgr(ILogger log)
        {
            _log = log;
        }

        public NxLogListenerCollection ApplicationListeners => throw new NotImplementedException();

        public NxLogListenerCollection SessionListeners => throw new NotImplementedException();

        public string BaseLogPathOrName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ILogMgr Clone()
        {
            throw new NotImplementedException();
        }

        public void Debug(string message, params string[] ObjectType)
        {
            _log.LogDebug(message);
        }

        public void Debug(Exception ex, params string[] ObjectType)
        {
            _log.LogDebug(ex.Message);
        }

        public void Error(string message, params string[] ObjectType)
        {
            _log.LogError(message);
        }

        public void Error(Exception ex, params string[] ObjectType)
        {
            _log.LogError(ex.Message, ex);
        }

        public void Info(string message, params string[] ObjectType)
        {
            _log.LogInformation(message);
        }

        public void Info(Exception ex, params string[] ObjectType)
        {
            _log.LogInformation(ex.Message);
        }

        public void Warning(string message, params string[] ObjectType)
        {
            _log.LogWarning(message);
        }

        public void Warning(Exception ex, params string[] ObjectType)
        {
            _log.LogWarning(ex.Message);
        }

        public void WriteLog(ILogData data)
        {
            throw new NotImplementedException();
        }

        public void WriteLog(string message, ILogMgr.SeverityLevel level, params string[] ObjectType)
        {
            switch (level)
            {
                case ILogMgr.SeverityLevel.Info:
                    Info(message);
                    break;
                case ILogMgr.SeverityLevel.Error:
                    Error(message);
                    break;
                case ILogMgr.SeverityLevel.Warning:
                    Warning(message);
                    break;
                default:
                    Debug(message);
                    break;
            }
        }

        public void WriteLog(Exception ex, ILogMgr.SeverityLevel level, params string[] ObjectType)
        {
            WriteLog(ex.Message, level);
        }
    }
}
