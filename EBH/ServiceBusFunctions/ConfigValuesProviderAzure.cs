﻿using Configuration;
using Configuration.Interfaces;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NextGen.Framework.Managers.StorageMgr;
using Constants = Configuration.Interfaces.Constants;

namespace _3E.Func
{
    public class ConfigValuesProviderAzure : IContainerCfgValueProvider
    {
        public ILogger Logger { get; }
       
        private StorageContainerCfgs _containers;
        private IConfigurationSource _configuration;

        private const string AppOutputContainerKey = "ApplicationOutput";
        private const string AttachmentsContainerKey = "Attachments";

        public ConfigValuesProviderAzure(ILogger logger)
        {
            Logger = logger;
        }
        public void ReadContainers(StorageContainerCfgs conts)
        {
            _containers = conts;

            var config = GetConfigurationAsync(Logger).GetAwaiter().GetResult();

            var appOutputConnStringSource = GetTrsEnabled(config)
                ? Constants.AppSettings.ApplicationOutputBlobConnectionStringFromTRS
                : Constants.AppSettings.APPLICATIONOUTPUTCONNECTIONSTRING;
            AddContainer(config, appOutputConnStringSource, Constants.AppSettings.APPLICATIONOUTPUTCONTAINERNAME, AppOutputContainerKey);

            var attachmentConnStringSource = GetTrsEnabled(config)
                ? Constants.AppSettings.AttachmentsBlobConnectionStringFromTRS
                : Constants.AppSettings.ATTACHMENTCONNECTIONSTRING;
            AddContainer(config, attachmentConnStringSource, Constants.AppSettings.ATTACHMENTCONTAINERNAME, AttachmentsContainerKey);
        }

        private void AddContainer(IConfigurationSource config, string connectionStringSource, string containerNameConst, string containerKey)
        {
            const string connStringKeyProp = "ConnectionString";
            const string containerNameProp = "ContainerName";

            var props = new Dictionary<string, string>();

            var connectionString = config.GetSettingAsync(connectionStringSource).GetAwaiter().GetResult();
            if (connectionString == null)
            {
                var message = $"{connectionStringSource} is not provided in the configuration.";
                Logger.LogCritical(message);
                throw new ConfigurationErrorsException(message);
            }

            var containerName = config.GetSettingAsync(containerNameConst).GetAwaiter().GetResult();
            if (containerName == null)
            {
                var message = $"{containerNameConst} is not provided in the configuration.";
                Logger.LogCritical(message);
                throw new ConfigurationErrorsException(message);
            }

            props.Add(connStringKeyProp, connectionString.Value);
            props.Add(containerNameProp, containerName.Value);
            _containers.AddContainer(new ContainerConfiguration(StorageType.Blob, containerKey, props));
        }

        private bool GetTrsEnabled(IConfigurationSource config)
        {
            var isTrsEnabled = true;
            var isTrsEnabledSetting = config.GetSettingAsync(Constants.AppSettings.TRSIsEnabled).GetAwaiter().GetResult();
            if (isTrsEnabledSetting == null)
            {
                Logger.LogWarning($"{Constants.AppSettings.TRSIsEnabled} is not provided in the configuration. Considered 'True' by default.");
            }
            else
            {
                bool.TryParse(isTrsEnabledSetting.Value, out isTrsEnabled);
            }

            return isTrsEnabled;
        }

        private async Task<IConfigurationSource> GetConfigurationAsync(ILogger logger)
        {
            if (this._configuration == null)
            {
                var configFactory = new ConfigurationSourceFactory(logger);
                this._configuration = await configFactory.Create().ConfigureAwait(false);
            }
            return this._configuration;
        }
    }
}
