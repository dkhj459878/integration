﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Exceptions
{
	[Serializable]
	public class EBHWebResponseException : WebException
	{
		public EBHWebResponseException() : base() { }
		public EBHWebResponseException(string message) : base(message) { }
		public EBHWebResponseException(string message, Exception inner) : base(message, inner) { }
		protected EBHWebResponseException(SerializationInfo info, StreamingContext context) { }

		public HttpStatusCode StatusCode { get; set; }
	}
}