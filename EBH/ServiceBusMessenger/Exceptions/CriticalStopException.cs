﻿using NextGen.Framework.Managers.LogMgr;
using System;
using System.Runtime.Serialization;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Exceptions
{
    [Serializable]
    public class CriticalStopException : Exception
    {
        const string STOPMSG = "Failed on EBH service, Stopping Services";

        public CriticalStopException()
            : base(STOPMSG)
        { }

        public CriticalStopException(Exception inner, ILogMgr logMgr)
            : base(STOPMSG, inner)
        {
            if (logMgr != null)
            {
                logMgr.Error("Critical fail, Stopping service");
                if (inner != null)
                    logMgr.Error(inner);
            }
        }

        public CriticalStopException(string errorMsg)
            : base(errorMsg)
        { }

        protected CriticalStopException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(STOPMSG)
        { }

        public CriticalStopException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}