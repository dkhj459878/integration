﻿using NextGen.Framework.Managers.LogMgr;
using System;
using System.Runtime.Serialization;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Exceptions
{
    [Serializable]
    public class CriticalWaitException : Exception
    {
        const string WAITMSG = "Failed on EBH service, Waiting for EBH";

        public CriticalWaitException()
            : base(WAITMSG)
        { }

        public CriticalWaitException(Exception inner, ILogMgr logMgr)
            : base(WAITMSG, inner)
        {
            if (logMgr != null)
            {
                logMgr.Error("Critical Wait, wait for healthcheck");
                if (inner != null)
                    logMgr.Error(inner);
            }
        }

        public CriticalWaitException(string errorMsg)
            : base(errorMsg)
        { }

        protected CriticalWaitException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(WAITMSG)
        { }

        public CriticalWaitException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}