﻿
namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Enums
{
	public enum RepeatedInvStatusParameter
	{
		SubmissionId,
		InvoiceId,
	}
}
