﻿using System;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Enums
{
    [Flags]
	public enum AdditionalCheckOptions
	{
		None = 0,
		FileStorage = 1,
		RestService = 2
	}
}
