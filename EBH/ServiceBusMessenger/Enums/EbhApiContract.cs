﻿
namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Enums
{
	public enum EbhApiContract
	{
		Submission,
		ValidationPost,
		ValidationGet,
		InvStatusGet,
		InvStatusUpdate,
		InvStatusGetHistory,
		Client,
		Vendor,
		HealthCheck,
		UploadAttachment,
		AddAttachment,
		CommitInvoice,
	}
}
