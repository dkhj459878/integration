﻿
namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Enums
{
	public enum InvStatusParameter
	{
		SubmissionId,
		InvoiceId,
		PayorOrgId
	}
}
