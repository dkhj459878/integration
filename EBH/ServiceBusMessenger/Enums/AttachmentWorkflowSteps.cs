﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Enums
{
    /// <summary>
    /// Defines sequential workflow for EBH Attachment API.
    /// <para>
    /// URL: https://ebillinghub.portal.azure-api.net/attachment-api
    /// </para>
    /// </summary>
    public enum AttachmentWorkflowSteps
    {
        /// <summary>
        /// Attachment is not sent to the API and/or invoice is submitted to EBH.
        /// </summary>
        None = 0,

        /// <summary>
        /// Attachment is sent to EBH Attachment API.
        /// <para>
        /// API: https://ebillinghub.portal.azure-api.net/attachment-api/
        /// </para>
        /// </summary>
        Step1,

        /// <summary>
        /// Attachment is associated with an invoice submission by Add Attachment EBH Submission API.
        /// <para>
        /// API: https://ebillinghub.portal.azure-api.net/submission-api#jive_content_id_Add_Attachment
        /// </para>
        /// </summary>
        Step2,

        /// <summary>
        /// Attachent has sent, associated. Submitted invoice is committed and ready to send. Operation performed by Commit a Submitted Invoice EBH Submission API.
        /// <para>
        /// API: https://ebillinghub.portal.azure-api.net/submission-api#jive_content_id_Commit_a_submitted_invoice
        /// </para>
        /// </summary>
        Step3
    }
}
