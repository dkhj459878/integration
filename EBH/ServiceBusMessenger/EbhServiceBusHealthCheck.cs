﻿using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Timers;
using Thread = System.Threading.Thread;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger
{
    public class EbhServiceBusHealthCheck
    {
        protected EbhWebService _ebhWebService;
        protected IServiceBusHandler _serviceBusHandler;
        protected Timer _timer;
        
        public EbhServiceBusHealthCheck(IServiceBusHandler serviceBusHandler, EbhWebService ebhWebService)
        {
            if (serviceBusHandler == null)
                throw new ArgumentNullException(nameof(serviceBusHandler));

            if (ebhWebService == null)
                throw new ArgumentNullException(nameof(ebhWebService));
                
            _ebhWebService = ebhWebService;
            _serviceBusHandler = serviceBusHandler;

            _timer = new Timer(60000);
            _timer.Elapsed += Timer_Elapsed;
        }

        ~EbhServiceBusHealthCheck()
        {
            _timer.Elapsed -= Timer_Elapsed;
            _ebhWebService = null;
        }

        public void EBHHealthCheckLoop()
        {
            _timer.Start();
            while (_timer.Enabled)
            {
                Thread.Sleep(2000);
            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_ebhWebService.HealthCheck())
            {
                _timer.Stop();
            }
        }

        public bool SBHealthCheck()
        {
            return _serviceBusHandler.IsConnected;
        }
    }
}
