﻿using System.Collections.Generic;
using System.Net;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Extensions
{
    using Enums;

    public static class EbhApiContractExtensions
	{
		private static readonly Dictionary<EbhApiContract, HttpStatusCode> ebhApiSuccessCode;

		static EbhApiContractExtensions()
		{
			ebhApiSuccessCode = new Dictionary<EbhApiContract, HttpStatusCode>
			{
				[EbhApiContract.Submission] = HttpStatusCode.Created,
				[EbhApiContract.ValidationPost] = HttpStatusCode.Created,
				[EbhApiContract.ValidationGet] = HttpStatusCode.OK,
				[EbhApiContract.InvStatusGet] = HttpStatusCode.OK,
				[EbhApiContract.InvStatusUpdate] = HttpStatusCode.OK,
				[EbhApiContract.InvStatusGetHistory] = HttpStatusCode.OK,
				[EbhApiContract.Client] = HttpStatusCode.OK,
				[EbhApiContract.Vendor] = HttpStatusCode.OK,
				[EbhApiContract.HealthCheck] = HttpStatusCode.OK,
				[EbhApiContract.UploadAttachment] = HttpStatusCode.Created,
				[EbhApiContract.AddAttachment] = HttpStatusCode.Created,
				[EbhApiContract.CommitInvoice] = HttpStatusCode.NoContent,
			};
		}

		public static HttpStatusCode GetSuccessfulApiStatusCode(this EbhApiContract ebhApi)
		{
			return ebhApiSuccessCode[ebhApi];
		}
	}
}
