﻿using NextGen.Framework.Managers.ServiceBusMgr;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Extensions
{
    public static class BrokeredMessageExtensions
	{
		/// <summary>
		/// Gets the property value.
		/// </summary>
		public static string Get(this IBrokeredMessage message, string propertyName)
		{
			if (message == null || message.Properties == null || !message.Properties.ContainsKey(propertyName))
				return string.Empty;

			return message.Properties[propertyName].ToString();
		}
	}
}
