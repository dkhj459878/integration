﻿

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger
{
    public interface IEbhWebServiceConfiguration
    {
        string Url { get; }
        string ProviderKey { get; }
        string RequestTimeout { get; }
    }

    public class EbhWebServiceConfiguration : IEbhWebServiceConfiguration
    {
        public const string EBHURL = "EBHUrl";
        public const string PROVIDERKEY = "ProviderKey";
        public const string EBHRESTSERVICETIMEOUT = "eBHRestServiceTimeout";

        public string Url => System.Environment.GetEnvironmentVariable(EBHURL);
        public string ProviderKey => System.Environment.GetEnvironmentVariable(PROVIDERKEY);
        public string RequestTimeout => System.Environment.GetEnvironmentVariable(EBHRESTSERVICETIMEOUT) ?? "40000";
    }
}