﻿using Newtonsoft.Json.Linq;
using NextGen.Framework.Managers.LogMgr;
using Polly;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger
{
    using Enums;
    using Exceptions;
    using Extensions;
    using Helper;
    using Interfaces;

    public class EbhWebService : IEbhWebService
    {
        const string uriDelimiter = "/";

        protected readonly int GetInterval;
        protected readonly int GetThreshold;

        private readonly ILogMgr _logger;
        private readonly Uri _ebhUrl;
        private readonly string _providerKey;
        private readonly string _eBHRestServiceTimeout;

        private EbhWebService()
        {
            GetInterval = 100;
            GetThreshold = 60000;
        }

        protected EbhWebService(IEbhWebServiceConfiguration configuration)
            : this()
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            var url = configuration.Url.EndsWith(uriDelimiter)
                ? configuration.Url
                : configuration.Url + uriDelimiter;

            var ok = Uri.TryCreate(url, UriKind.Absolute, out _ebhUrl);
            _providerKey = configuration.ProviderKey;
            _eBHRestServiceTimeout = configuration.RequestTimeout;
        }

        public EbhWebService(IEbhWebServiceConfiguration configuration, ILogMgr logMgr)
            : this(configuration)
        {
            _logger = logMgr ?? Globals.LogMgr ?? throw new ArgumentNullException(nameof(logMgr), "Cannot inject the ILogMgr object.");
        }

        public virtual string PostValidationRequest(string username, string producerId, string document, string documentType)
        {
            //send to ebh to validate
            var postUri = new Uri(_ebhUrl, "validations/api/v1/ValidationRequest");
            var headers = new Dictionary<string, string>
            {
                {"ProducerId", producerId},
                {"Provider", username},
                {"Username", username},
                {"ProviderKey", _providerKey},
                {"InvoiceType", documentType}
            };
            // POST
            var request = CreateWebRequest(postUri, "application/xml", HttpMethod.Post, headers);

            SetRequestContent(request, document);
            LogCall(nameof(PostValidationRequest), HttpMethod.Post);
            var postResponseStream = Send(request);
            string postResponse = ReadResponseStream(postResponseStream, EbhApiContract.ValidationPost);

            var trackingId = ExtractField<string>(postResponse, "ID");
            return trackingId;
        }

        public const string GETCLIENTS = "GETCLIENTS";
        public const string GETVENDORS = "GETVENDORS";

        private Dictionary<string, string> urlMapping = new Dictionary<string, string>
        {
            {GETCLIENTS, "clients/api/v1/client"},
            {GETVENDORS, "vendors/api/v1/vendor"}
        };

        private EbhApiContract ResolveApiContractForGetData(string dataType)
        {
            EbhApiContract apiContract;
            if (dataType == GETVENDORS) apiContract = EbhApiContract.Vendor;
            else if (dataType == GETCLIENTS) apiContract = EbhApiContract.Client;
            else throw new NotSupportedException($"{dataType} is not supported for GETData method");

            return apiContract;
        }

        public virtual string GetData(string dataType, string username, string producerId)
        {
            var getUri = new Uri(_ebhUrl, urlMapping[dataType]);
            var headers = new Dictionary<string, string>
            {
                {"ProducerId", producerId},
                {"Provider", username},
                {"Username", username},
                {"ProviderKey", _providerKey}
            };

            // GET
            var request = CreateWebRequest(getUri, "application/json", HttpMethod.Get, headers);

            LogCall($"{nameof(GetData)}/{dataType}", request.Method);
            HttpWebResponse getResponseStream = Send(request);
            var getResponse = ReadResponseStream(getResponseStream, ResolveApiContractForGetData(dataType));

            return getResponse;
        }

        public virtual string PostInvoiceStatusRequest(string username, string producerId, string body)
        {
            //send to ebh to get invoices status
            var getUri = new Uri(_ebhUrl, "statuses/api/v1/search");
            var headers = new Dictionary<string, string>
            {
                {"ProducerId", producerId},
                {"Provider", username},
                {"Username", username},
                {"ProviderKey", _providerKey}
            };
            // POST
            var request = CreateWebRequest(getUri, "application/json", HttpMethod.Post, headers);

            SetRequestContent(request, body);
            LogCall(nameof(PostInvoiceStatusRequest), request.Method);
            var postResponseStream = Send(request);
            string postResponse = ReadResponseStream(postResponseStream, EbhApiContract.InvStatusGet);

            return postResponse;
        }

        public virtual string GetInvoiceStatusHistory(string username, string producerId, Dictionary<string, object> parameters)
        {
            //send to ebh to get invoices status history
            var requestUri = _ebhUrl + "statuses/api/v1/history";
            requestUri = AddParametersToQueryString(requestUri, parameters);
            var getUri = new Uri(requestUri);
            var headers = new Dictionary<string, string>
            {
                {"ProducerId", producerId},
                {"Provider", username},
                {"Username", username},
                {"ProviderKey", _providerKey}
            };
            // GET
            var request = CreateWebRequest(getUri, "application/json", HttpMethod.Get, headers);

            LogCall(nameof(GetInvoiceStatusHistory), request.Method);
            var getResponseStream = Send(request);
            string getResponse = ReadResponseStream(getResponseStream, EbhApiContract.InvStatusGetHistory);

            return getResponse;
        }

        public virtual string PutInvoiceStatusRequest(string username, string producerId, Dictionary<string, object> parameters, string body)
        {
            //send to ebh to update invoice statuses
            var requestUri = _ebhUrl + "statuses/api/v1/invoice";
            requestUri = AddParametersToQueryString(requestUri, parameters);
            var putUri = new Uri(requestUri);
            var headers = new Dictionary<string, string>
            {
                {"ProducerId", producerId},
                {"Provider", username},
                {"Username", username},
                {"ProviderKey", _providerKey}
            };
            // PUT
            var request = CreateWebRequest(putUri, "application/json", HttpMethod.Put, headers);
            SetRequestContent(request, body);
            LogCall(nameof(PutInvoiceStatusRequest), request.Method);
            var putResponseStream = Send(request);
            string putResponse = ReadResponseStream(putResponseStream, EbhApiContract.InvStatusUpdate);

            return putResponse;
        }

        public virtual string PostInvoiceSubmissionRequest(string username, string producerId, string document, string documentType)
        {
            //send to ebh to submission      submissions/api/v1/ValidationRequests
            var postUri = new Uri(_ebhUrl, "submissions/api/v1/SubmissionRequests");
            var headers = new Dictionary<string, string>
            {
                {"ProducerId", producerId},
                {"Provider", username},
                {"Username", username},
                {"ProviderKey", _providerKey},
                {"InvoiceType", documentType}
            };
            // POST
            var request = CreateWebRequest(postUri, "application/xml", HttpMethod.Post, headers);

            SetRequestContent(request, document);
            LogCall(nameof(PostInvoiceSubmissionRequest), request.Method);
            var postResponseStream = Send(request);
            string postResponse = ReadResponseStream(postResponseStream, EbhApiContract.Submission);

            return postResponse;
        }

        /// <summary>
        /// Creates POST-request, writes binary body and uploads an attachment for EBH storage.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="producerId"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public virtual HttpWebResponse PostAttachmentRequest(string username, string producerId, string fileName, Stream stream)
        {
            // {eBHDomain}/attachments/api/v1/attachment
            var postUri = new Uri(_ebhUrl, "attachments/api/v1/attachment");

            var headers = new Dictionary<string, string>
            {
                // ProducerId	Required. Specifies the client on behalf of which the request is sent.
                {"ProducerId", producerId},
                {"Provider", username},
                // ProviderKey Required. Specifies the access token.
                {"ProviderKey", _providerKey},
                // Content-Disposition Required. Describes the attachments file name. The accepted format is as follows: attachment; filename = "{filename}".The file name can have up to 200 characters.
                // The following file extensions are allowed: PDF,DOC,DOCX,TIF,XLSX,MSG,PNG,TXT,TIFF,XLS,JPG,XML,RTF,LEDES,XPS,XLTX,HTM,XLSB,BMP,CSV,HTML,PPTX,XLSM,JPEG.
                {"Content-Disposition", $"attachment; filename = \"{fileName}\""},
                // Username Optional. Specifies the user on behalf of which the request is sent. The value is used for reporting purposes only and defined by the caller.
                {"Username", username},
            };

            var request = CreateWebRequest(postUri, "application/octet-stream", HttpMethod.Post, headers);

            SetRequestContent(request, stream);
            LogCall(nameof(PostAttachmentRequest), request.Method);
            return Send(request);
        }

        public virtual HttpWebResponse PostAddAttachmentRequest(string username, string producerId, string submissionId, string id, string body)
        {
            // {eBHDomain}/submissions/api/v1/SubmissionRequests/{SubmissionId}/invoices/{Id}/attachments
            var postUri = new Uri(_ebhUrl, $"submissions/api/v1/SubmissionRequests/{submissionId}/invoices/{id}/attachments");

            var headers = new Dictionary<string, string>
            {
                {"ProducerId", producerId},
                {"ProviderKey", _providerKey},
                {"Username", username},
            };

            var request = CreateWebRequest(postUri, "application/json", HttpMethod.Post, headers);
            SetRequestContent(request, body);
            LogCall(nameof(PostAddAttachmentRequest), request.Method);
            return Send(request);
        }

        public virtual HttpWebResponse PutCommitSubmittedInvoiceRequest(string username, string producerId, string submissionId, string id)
        {
            // PUT {eBHDomain}/submissions/api/v1/SubmissionRequests/{SubmissionId}/invoices/{Id}/ready
            var putUri = new Uri(_ebhUrl, $"submissions/api/v1/SubmissionRequests/{submissionId}/invoices/{id}/ready");

            var headers = new Dictionary<string, string>
            {
                {"ProducerId", producerId},
                {"ProviderKey", _providerKey},
                {"Username", username},
            };

            const int maxRetries = 7, startSeconds = 1, timeoutSeconds = 30;
            var policy = Policy
                .Handle<EBHWebResponseException>(e => e.StatusCode == HttpStatusCode.BadRequest
                    && e.Message.Contains("Submission request has been failed or is in progress."))
                .Or<WebException>(e => e.Status == WebExceptionStatus.UnknownError
                    || e.Response != null && e.Response is HttpWebResponse && ((HttpWebResponse)e.Response).StatusCode == HttpStatusCode.BadRequest)
                .WaitAndRetry(maxRetries, // No more than 7 times. No longer than 30 seconds of waiting total time
                    retryAttempt => LinearSleepDuration(1000D * startSeconds, retryAttempt), // 1, 2, 3, ..., 7 seconds. Max waiting time: n*(n+1)/2 = 7*(7+1)/2 = 7*8/2 = 28 seconds
                    (exception, timeSpan, retryCount, context) => LogWaitAndRetry(nameof(PutCommitSubmittedInvoiceRequest), exception, timeSpan, retryCount, context))
                .WithPolicyKey($"{nameof(PutCommitSubmittedInvoiceRequest)}-{nameof(RetrySyntax.WaitAndRetry)}");

            Func<HttpWebResponse> action = () =>
            {
                var request = CreateWebRequest(putUri, "application/json", HttpMethod.Put, headers);
                request.Timeout = 1000 * timeoutSeconds; // 30 seconds
                SetRequestContent(request, string.Empty);
                LogCall(nameof(PutCommitSubmittedInvoiceRequest), request.Method);
                return Send(request);
            };
            return ApplyPolicy<HttpWebResponse>(policy, action);
        }

        private TResult ApplyPolicy<TResult>(Policy policy, Func<TResult> action)
        {
            _logger.Info($"{nameof(EbhWebService)}.{nameof(Send)}: Applying policy {policy.PolicyKey}...");
            var watch = new Stopwatch();
            watch.Start();

            var result = policy.Execute<TResult>(action);

            watch.Stop();
            _logger.Info($"{nameof(EbhWebService)}.{nameof(Send)}: Exitting policy {policy.PolicyKey} after execution in {watch.ElapsedMilliseconds} milliseconds.");
            return result;
        }

        private TimeSpan LinearSleepDuration(double startingMilliseconds, int retryAttempt)
        {
            return TimeSpan.FromMilliseconds(startingMilliseconds * retryAttempt);
        }

        private void LogWaitAndRetry(string clientName, Exception exception, TimeSpan timeSpan, int retryCount, Context context)
        {
            _logger.Warning($"{nameof(EbhWebService)}.{clientName}: Policy '{context.PolicyKey}'. Current sleep duration: {timeSpan.TotalMilliseconds} milliseconds. Retry count: {retryCount}.");
            if (exception is WebException)
            {
                var error = exception as WebException;
                _logger.Warning($"{nameof(EbhWebService)}.{clientName}: Previous client's action execution was failed. Status: {error.Status}. Reason: {error.Message}.");
            }
            LogError(exception);
        }

        /// <summary>
        /// Validate EBH Service is up and running
        /// </summary>
        /// <returns></returns>
        public virtual bool HealthCheck()
        {
            var result = HealthCheck(null);
            return result.Key;
        }

        /// <summary>
        /// Sends dummy request to EBH to make sure that EbhUrl and ProviderKey are right.
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public virtual KeyValuePair<bool, string> HealthCheck(Uri uri)
        {
            //Error "HTTP Header ProducerId must be provided and be a number" returned by EBH
            const string missingProducerId = "ProducerId must be provided";

            //Use ValidationRequest, send last ubf
            var esf = "<esfdoc></esfdoc>";
            var getUri = uri ?? new Uri(_ebhUrl, "validations/api/v1/ValidationRequest");
            var headers = new Dictionary<string, string>
            {
                {"ProviderKey", _providerKey}
            };
            // POST
            var request = CreateWebRequest(getUri, "application/xml", HttpMethod.Post, headers);

            HttpWebResponse postResponseStream;
            try
            {
                SetRequestContent(request, esf);
                LogCall(nameof(HealthCheck), request.Method);
                postResponseStream = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                try
                {
                    CheckExceptionSeverity(ex);
                }
                catch (CriticalWaitException)
                {
                    return new KeyValuePair<bool, string>(false, getUri + Environment.NewLine + ex.Message);
                }
                catch (CriticalStopException)
                {
                    return new KeyValuePair<bool, string>(false, getUri + Environment.NewLine + ex.Message);
                }
                catch (Exception e) when (e.Message.IndexOf(missingProducerId, StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    //Health check message doesn't send ProducerId along with the message so this exception is ok
                    //Don't send any message back in order to not confuse users
                    return new KeyValuePair<bool, string>(true, getUri.ToString());
                }
                catch (Exception e)
                {
                    return new KeyValuePair<bool, string>(false, getUri + Environment.NewLine + e.Message);
                }
                return new KeyValuePair<bool, string>(true, getUri + Environment.NewLine + ex.Message);
            }
            catch (Exception ex)
            {
                return new KeyValuePair<bool, string>(false, getUri + Environment.NewLine + ex.Message);
            }
            return new KeyValuePair<bool, string>(true, String.Empty);
        }

        private void SetRequestContent(HttpWebRequest req, string body)
        {
            if (string.IsNullOrEmpty(body))
            {
                req.ContentLength = 0;
                return;
            }

            var byteArray = Encoding.UTF8.GetBytes(body);
            using (var source = new MemoryStream(byteArray))
            {
                req.ContentLength = byteArray.Length;
                SetRequestContent(req, source);
            }
        }

        private void SetRequestContent(HttpWebRequest req, Stream stream)
        {
            long length = 0;
            string status = string.Empty;
            try
            {
                stream.Seek(0, SeekOrigin.Begin);
                length = req.ContentLength = stream.Length;
                using (var bodyStream = req.GetRequestStream())
                {
                    stream.CopyTo(bodyStream);
                }
                status = "Success";
            }
            catch (Exception e)
            {
                status = "Failed";
                LogError(e);
                throw;
            }
            finally
            {
                _logger.Info($"{nameof(EbhWebService)}.{nameof(SetRequestContent)}: {status} to set request body content of length in {length} bytes.");
            }
        }

        private string AddToQueryString(string requestUri, string key, string value)
        {
            var uriBuilder = new UriBuilder(requestUri);

            if (!(string.IsNullOrEmpty(key) || string.IsNullOrEmpty(value)))
            {
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);

                query.Add(key, value);

                uriBuilder.Query = query.ToString();
            }

            return uriBuilder.ToString();
        }

        private string AddParametersToQueryString(string requestUri, Dictionary<string, object> parameters)
        {
            foreach (var parameter in parameters)
            {
                if (Enum.GetNames(typeof(RepeatedInvStatusParameter)).Contains(parameter.Key, StringComparer.InvariantCultureIgnoreCase))
                {
                    foreach (var value in parameter.Value.ToString().Split(';'))
                    {
                        requestUri = AddToQueryString(requestUri, parameter.Key, value);
                    }
                }
                else
                {
                    requestUri = AddToQueryString(requestUri, parameter.Key, parameter.Value.ToString());
                }
            }

            return requestUri;
        }

        private HttpWebRequest CreateWebRequest(Uri uri, string contentType, HttpMethod httpMethod, Dictionary<string, string> additionalHeaders)
        {
            var headers = additionalHeaders ?? new Dictionary<string, string>();
            var headersInfo = headers.Select(pair => $"{pair.Key}: '{pair.Value}'");
            _logger.Info(new StringBuilder()
                .Append($"{nameof(EbhWebService)}.{nameof(CreateWebRequest)}: ")
                .Append($"{nameof(uri)}: {uri}; ")
                .Append($"{nameof(contentType)}: {contentType}; ")
                .Append($"{nameof(httpMethod)}: {httpMethod}; ")
                .Append(string.Join("; ", headersInfo))
                .ToString());

            var request = WebRequest.CreateHttp(uri);
            request.ContentType = contentType;
            request.Method = httpMethod.ToString();
            request.Timeout = Convert.ToInt32(_eBHRestServiceTimeout);
            foreach (var header in additionalHeaders)
            {
                request.Headers.Add(header.Key, header.Value);
            }
            return request;
        }

        public virtual HttpWebResponse TryGetRequest(Uri getUri, Dictionary<string, string> headers)
        {
            var request = CreateWebRequest(getUri, "application/json", HttpMethod.Get, headers);
            return Send(request);
        }

        private HttpWebResponse Send(HttpWebRequest request)
        {
            _logger.Info($"{nameof(EbhWebService)}.{nameof(Send)}: {request.Method} to {request.RequestUri.PathAndQuery}");
            try
            {
                return (HttpWebResponse)request.GetResponse();
            }
            catch (WebException exc)
            {
                CheckExceptionSeverity(exc);
            }
            catch (Exception exc)
            {
                LogError(exc);
                throw exc;
            }
            return null;
        }

        public virtual string GetValidationResultByTrackingId(string username, string producerId, string trackingId)
        {
            // GET
            var getUri = new Uri(_ebhUrl, $"validations/api/v1/ValidationRequest/{trackingId}");
            var headers = new Dictionary<string, string>
            {
                {"ProducerId", producerId},
                {"Provider", username},
                {"Username", username},
                {"ProviderKey", _providerKey}
            };
            LogCall(nameof(GetValidationResultByTrackingId), HttpMethod.Get);
            var getResponse = PullValidatedResponse(getUri, headers);
            if (string.IsNullOrEmpty(getResponse))
            {
                var errMsg = "No reponse from Polling";
                LogError(errMsg);
                throw new Exception(errMsg);
            }
            var validationStatus = ExtractField<int>(getResponse, "Status");
            var validationMessage = ExtractField<string>(getResponse, "ValidationMessage");
            if (string.IsNullOrEmpty(validationMessage))
                validationMessage = getResponse;

            if (validationStatus != 4)
            {
                var errMsg = $"Error Occured in Getting UBF -  Error in GET Validations, Status is not validated [{validationStatus}]. Message: " + validationMessage;
                LogError(errMsg);
                throw new Exception(errMsg);
            }
            return getResponse;
        }

        private string PullValidatedResponse(Uri getUri, Dictionary<string, string> headers)
        {
            var sw = new Stopwatch();
            sw.Start();
            HttpWebResponse getResponseStream = TryGetRequest(getUri, headers);
            while (getResponseStream != null && (getResponseStream.StatusCode == HttpStatusCode.Accepted) && (sw.ElapsedMilliseconds < GetThreshold))
            {
                Task.Delay(GetInterval);
                getResponseStream.Close();
                getResponseStream = TryGetRequest(getUri, headers);
            }

            sw.Stop();

            return ReadResponseStream(getResponseStream, EbhApiContract.ValidationGet);
        }

        public string ReadResponseStream(HttpWebResponse response, EbhApiContract ebhApi)
        {
            if (response == null)
                return string.Empty;

            if (!IsRequestSuccessful(response, ebhApi))
            {
                string errMsg = GetErrrorMsgOnRequest(response, ebhApi);
                LogError(errMsg);
                throw new EBHWebResponseException(errMsg) { StatusCode = response.StatusCode };
            }

            using (var getStreamReader = new StreamReader(response.GetResponseStream(), true))
            {
                return getStreamReader.ReadToEnd();
            }
        }

        private bool IsRequestSuccessful(HttpWebResponse responseStream, EbhApiContract ebhApi)
        {
            var statusCode = ebhApi.GetSuccessfulApiStatusCode();
            return responseStream.StatusCode == statusCode;
        }

        private string GetErrrorMsgOnRequest(HttpWebResponse responseStream, EbhApiContract ebhApi)
        {
            var ebhApiErrorMsg = new Dictionary<EbhApiContract, string>
            {
                [EbhApiContract.Submission] = $"EBHSender: Error Occured in Submitting ESF - did not return the expected status for POST Request. Returned status code {responseStream.StatusCode} . Should be {HttpStatusCode.Created}",
                [EbhApiContract.ValidationPost] = $"EBHSender: Error Occured in Validating ESF - did not return the expected status for POST Request. Returned status code {responseStream.StatusCode} . Should be {HttpStatusCode.Created}",
                [EbhApiContract.ValidationGet] = $"EBHSender: Error Occured in Validating ESF - did not return the expected status for GET Request. Returned status code {responseStream.StatusCode} . Should be {HttpStatusCode.OK}",
                [EbhApiContract.InvStatusGet] = $"Error Occured Reading Response - did not return the expected status for Invoice Status POST Request. Returned status code {responseStream.StatusCode}. Should be {HttpStatusCode.OK}",
                [EbhApiContract.InvStatusUpdate] = $"Error Occured Reading Response - did not return the expected status for Invoice Status Update GET Request. Returned status code {responseStream.StatusCode}. Should be {HttpStatusCode.OK}",
                [EbhApiContract.InvStatusGetHistory] = $"Error Occured Reading Response - did not return the expected status for Invoice History GET Request. Returned status code {responseStream.StatusCode}. Should be {HttpStatusCode.OK}",
                [EbhApiContract.Client] = $"Error Occured Reading Response - did not return the expected status for Client GET Request. Returned status code {responseStream.StatusCode}. Should be {HttpStatusCode.OK}",
                [EbhApiContract.Vendor] = $"Error Occured Reading Response - did not return the expected status for Vendor GET Request. Returned status code {responseStream.StatusCode}. Should be {HttpStatusCode.OK}",
                [EbhApiContract.UploadAttachment] = $"Error Occured Reading Response - did not return the expected status for Attachment API POST Request. Returned status code {responseStream.StatusCode}. Should be {HttpStatusCode.Created}",
                [EbhApiContract.AddAttachment] = $"Error Occured Reading Response - did not return the expected status for Submission Add Attachment API POST Request. Returned status code {responseStream.StatusCode}. Should be {HttpStatusCode.Created}",
                [EbhApiContract.CommitInvoice] = $"Error Occured Reading Response - did not return the expected status for Commit Submitted Invoice API PUT Request. Returned status code {responseStream.StatusCode}. Should be {HttpStatusCode.NoContent}",
            };

            return ebhApiErrorMsg[ebhApi];
        }

        private void LogError(Exception ex)
        {
            _logger.Error(ex);
            if (ex.InnerException != null)
                LogError(ex.InnerException);
        }

        private void LogError(string msg)
        {
            _logger.Error(msg);
        }

        private void LogCall(string callInfo, HttpMethod httpMethod = null)
        {
            var method = httpMethod ?? HttpMethod.Get;
            LogCall(callInfo, method.ToString());
        }

        private void LogCall(string callInfo, string httpVerb)
        {
            _logger.Info($"{nameof(EbhWebService)}.{callInfo}: {httpVerb ?? "GET"} web request is ready for sending.");
        }

        public static T ExtractField<T>(string inputString, string field)
        {
            var resultJObject = JObject.Parse(inputString);
            return resultJObject.GetValue(field, StringComparison.OrdinalIgnoreCase).Value<T>();
        }

        private void CheckExceptionSeverity(WebException ex)
        {
            Exception e;
            if (ex.Response == null)
            {
                e = new Exception(ex.Message, ex);
                LogError(e);
                throw new CriticalWaitException(ex, _logger);
            }

            HttpWebResponse postResponseStream = (HttpWebResponse)ex.Response;

            switch (postResponseStream.StatusCode)
            {
                //critical-wait, stop processing messages and go to wait pattern
                case HttpStatusCode.ServiceUnavailable:
                case HttpStatusCode.InternalServerError:
                    e = new Exception(postResponseStream.StatusDescription, ex);
                    LogError(e);
                    throw new CriticalWaitException(ex, _logger);

                //critical-stop, stop service
                case HttpStatusCode.Unauthorized:
                case HttpStatusCode.MethodNotAllowed:
                case HttpStatusCode.Forbidden:
                    e = new Exception(postResponseStream.StatusDescription, ex);
                    LogError(e);
                    throw new CriticalStopException(ex, _logger);

                //non-critical, log and move on
                //case HttpStatusCode.BadRequest:
                //case HttpStatusCode.NotAcceptable:
                //case HttpStatusCode.RequestEntityTooLarge:
                //case HttpStatusCode.UnsupportedMediaType:
                default:
                    Stream contentStream = postResponseStream.GetResponseStream();
                    string additionalErrorInfo = String.Empty;
                    if (contentStream != null)
                    {
                        using (var postStreamReader = new StreamReader(contentStream, true))
                        {
                            additionalErrorInfo = Environment.NewLine + postStreamReader.ReadToEnd();
                        }
                    }

                    e = new EBHWebResponseException(postResponseStream.StatusDescription + additionalErrorInfo, ex)
                    {
                        StatusCode = postResponseStream.StatusCode
                    };

                    LogError(e);
                    throw e;
            }
        }
    }
}