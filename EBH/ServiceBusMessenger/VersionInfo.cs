﻿namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger
{
    /// <summary>
    /// Version info will be updated during CI build.
    /// <para>We need these properties in order safely have access to version in RealTime for both Azure and OnPrem versions.</para>
    /// </summary>
    public class VersionInfo
    {
        public static string ServiceVersion = "3.0.0.0";
        public static string Revision = "0000";
    }
}