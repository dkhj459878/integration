﻿
namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Queues
{
    public class QueueNames
    {
        public const string Namespace = "TE3ESB";
        public const string EbhValidateRequest = nameof(Queues.EBHVALIDATEREQUEST);
        public const string EbhValidatePending = nameof(Queues.EBHVALIDATEPENDING);
        public const string EbhValidateResponse = nameof(Queues.EBHVALIDATERESPONSE);
        public const string EbhConfigurationRequest = nameof(Queues.EBHCONFIGURATIONREQUEST);
        public const string EbhConfigurationResponse = nameof(Queues.EBHCONFIGURATIONRESPONSE);
        public const string EbhInvoiceStatusRequest = nameof(Queues.EBHINVSTATREQUEST);
        public const string EbhInvoiceStatusResponse = nameof(Queues.EBHINVSTATRESPONSE);
        public const string EbhInvoiceSubmissionRequest = nameof(Queues.EBHINVSUBREQUEST);
        public const string EbhInvoiceSubmissionResponse = nameof(Queues.EBHINVSUBRESPONSE);
        public const string EbhAttachmentRequest = nameof(Queues.EBHATTACHMENTREQUEST);
        public const string EbhAttachmentResponse = nameof(Queues.EBHATTACHMENTRESPONSE);
    }
}
