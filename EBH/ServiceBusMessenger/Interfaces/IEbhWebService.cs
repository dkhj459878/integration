﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Interfaces
{
    using Enums;

    public interface IEbhWebService
    {
        string GetData(string dataType, string username, string producerId);
        string GetInvoiceStatusHistory(string username, string producerId, Dictionary<string, object> parameters);
        string GetValidationResultByTrackingId(string username, string producerId, string trackingId);
        bool HealthCheck();
        KeyValuePair<bool, string> HealthCheck(Uri uri);
        HttpWebResponse PostAttachmentRequest(string username, string producerId, string fileName, Stream stream);
        HttpWebResponse PostAddAttachmentRequest(string username, string producerId, string submissionId, string id, string body);
        HttpWebResponse PutCommitSubmittedInvoiceRequest(string username, string producerId, string submissionId, string id);
        string PostInvoiceStatusRequest(string username, string producerId, string body);
        string PostInvoiceSubmissionRequest(string username, string producerId, string document, string documentType);
        string PostValidationRequest(string username, string producerId, string document, string documentType);
        string PutInvoiceStatusRequest(string username, string producerId, Dictionary<string, object> parameters, string body);
        HttpWebResponse TryGetRequest(Uri getUri, Dictionary<string, string> headers);
        string ReadResponseStream(HttpWebResponse response, EbhApiContract ebhApi);
    }
}