﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Generic;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Messengers
{
    using Exceptions;
    using Helper;

    public class EbhServiceBusValidationResultReciever : EbhServiceBusMessenger
    {
        public EbhServiceBusValidationResultReciever(IServiceBusHandler sbh, EbhWebService ebhh, NxIOHelper ioh, ILogMgr log)
            : base(sbh, ebhh, ioh, log)
        { }

        public override string MessengerName => nameof(EbhServiceBusValidationResultReciever);

        public override string ProcessMessage(IBrokeredMessage msg)
        {
            if (string.IsNullOrEmpty(base.ProcessMessage(msg)))
                return string.Empty;

            var user = GetMsgPropertyValue(msg, USERNAMEID);
            var trackingId = GetMsgPropertyValue(msg, TRACKINGID);
            var producer = GetMsgPropertyValue(msg, PRODUCERID);
            _logger.Info($"{MessengerName}: Message received, TrackID: {trackingId}");
            if (string.IsNullOrEmpty(trackingId))
            {
                //failed
                SendResponseErrorMsg(msg, $"{MessengerName}: TrackingID not found on ValidatePending message.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }

            string getUbfResponse = string.Empty;
            try
            {
                getUbfResponse = _ebhWebService.GetValidationResultByTrackingId(user, producer, trackingId);
            }
            catch (Exception ex)
            {
                SendResponseErrorMsg(msg, ex.Message);
                _serviceBusHandler.TryCompleteMsg(msg);
                throw;
            }
            
            if (string.IsNullOrEmpty(getUbfResponse))
            {
                //failed
                SendResponseErrorMsg(msg, $"{MessengerName}: No response from EBH ValidationResult poll.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            var fileName = SendResponseQueue(msg, getUbfResponse);
            if (string.IsNullOrEmpty(fileName))
            {
                //failed
                SendResponseErrorMsg(msg, $"{MessengerName}: ValidationResult File not created.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            _serviceBusHandler.TryCompleteMsg(msg);

            return fileName;
        }

        private string SendResponseQueue(IBrokeredMessage curMsg, string flatFile)
        {
            if (curMsg == null)
                return string.Empty;
            
            curMsg.RenewLock();
            //write output file
            var trackingId = GetMsgPropertyValue(curMsg, TRACKINGID);
            var fileName = $"ValInv_{trackingId}.json";
            var pi = _ioHelper.WriteOutputFile(fileName, flatFile);
            //send result to Response
            fileName = pi.Combine();
            SendResponseMessage(curMsg, new Dictionary<string, object>
            {
                { RESPONSEFILEID, fileName },
            });

            return fileName;
        }
    }
}
