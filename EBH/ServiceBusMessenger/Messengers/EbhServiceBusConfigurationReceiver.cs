﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Generic;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Messengers
{
    using Exceptions;
    using Helper;

    public class EbhServiceBusConfigurationReceiver : EbhServiceBusMessenger
    {
        public EbhServiceBusConfigurationReceiver(IServiceBusHandler sbh, EbhWebService ebhh, NxIOHelper ioh, ILogMgr log)
            : base(sbh, ebhh, ioh, log)
        { }

        public override string MessengerName => nameof(EbhServiceBusConfigurationReceiver);

        public override string ProcessMessage(IBrokeredMessage msg)
        {
            if (string.IsNullOrEmpty(base.ProcessMessage(msg)))
                return string.Empty;

            var userName = GetMsgPropertyValue(msg, USERNAMEID);
            var producerID = GetMsgPropertyValue(msg, PRODUCERID);
            var configurationType = GetMsgPropertyValue(msg, CONFIGTYPE);
            _logger.Info($"{MessengerName}: Message received, ProducerID: {producerID}");
            if (string.IsNullOrEmpty(producerID))
            {
                //cannot continue, missing required data
                var errMsg = $"{MessengerName}: Could not GET {configurationType}, missing 'producerId'.";
                
                SendResponseErrorMsg(msg, errMsg);
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            string configData = string.Empty;
            try
            {
                configData = _ebhWebService.GetData(configurationType,userName, producerID);
            }
            catch (Exception ex)
            {
                SendResponseErrorMsg(msg, ex.Message);
                _serviceBusHandler.TryCompleteMsg(msg);
                throw;
            }
            
            if (string.IsNullOrEmpty(configData))
            {
                //failed
                SendResponseErrorMsg(msg, $"{MessengerName}: Could not GET {configurationType}.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }

            var fileName = SendConfigurationResponseQueue(msg, configData);
            if (string.IsNullOrEmpty(fileName))
            {
                //failed
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            _serviceBusHandler.TryCompleteMsg(msg);
            return fileName;
        }

        private string SendConfigurationResponseQueue(IBrokeredMessage curMsg, string configData)
        {
            if (curMsg == null)
                return string.Empty;
            curMsg.RenewLock();
            var producerID = GetMsgPropertyValue(curMsg, PRODUCERID);
            var configurationType = GetMsgPropertyValue(curMsg, CONFIGTYPE);
            string fileID = curMsg.MessageId;
            if (string.IsNullOrEmpty(fileID))
                fileID = Guid.NewGuid().ToString();
            var fileName = $"{configurationType}_{producerID}_{fileID}.json";
            var pi = _ioHelper.WriteOutputFile(fileName, configData);
            fileName = pi.Combine();
            //Push esb config response msg
            var props = new Dictionary<string, object>
            {
                { RESPONSEFILEID, fileName },
            };
            SendResponseMessage(curMsg, props);

            return fileName;
        }
    }
}
