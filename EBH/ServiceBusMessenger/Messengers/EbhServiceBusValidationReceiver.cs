﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.XPath;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Messengers
{
    using Helper;

    public class EbhServiceBusValidationReceiver : EbhServiceBusMessenger
    {
        public EbhServiceBusValidationReceiver(IServiceBusHandler sbh, EbhWebService ebhh, NxIOHelper ioh, ILogMgr log)
            : base(sbh, ebhh, ioh, log)
        { }

        public override string MessengerName => nameof(EbhServiceBusValidationReceiver);

        protected override string HealthCheck(IBrokeredMessage message)
        {
            SendResponseMessage(message, new Dictionary<string, object>
            {
                { EbhHealthCheckService.HEALTHCHECK, EbhHealthCheckService.HEALTHCHECK },
            });
            return string.Empty;
        }

        /// <summary>
        /// Receives ValidateRequest msg, send new msg to Pending
        /// </summary>
        /// <param name="msg"></param>
        public override string ProcessMessage(IBrokeredMessage msg)
        {
            if (string.IsNullOrEmpty(base.ProcessMessage(msg)))
                return string.Empty;

			//Read the file
			var filePath = GetMsgPropertyValue(msg, LOADFILEPATHID);
            _logger.Info($"{MessengerName}: Message received, File: '{filePath}'.");
            if (string.IsNullOrEmpty(filePath))
            {
                SendResponseErrorMsg(msg, $"{MessengerName}: No file load file specified.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            var document = _ioHelper.ReadLoadFile(filePath);
            if (string.IsNullOrEmpty(document))
            {
                //failed
                SendResponseErrorMsg(msg, $"{MessengerName}: Could not read file: '{filePath}'.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            //Post
            string trackingId = string.Empty;

            try
            {
                trackingId = ValidateInvoice(msg, document);
            }
            catch (Exception ex)
            {
                LastError = ex;
                SendResponseErrorMsg(msg, ex.Message);
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty; // throw caused hang and timeout on client side.
            }

            if (string.IsNullOrEmpty(trackingId))
            {
                //failed
                SendResponseErrorMsg(msg, $"{MessengerName}: No TrackingID returned from EBH.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            //Put esb pending msg
            var props = new Dictionary<string, object>
            {
                { TRACKINGID, trackingId },
            };
            SendResponseMessage(msg, props);

            _serviceBusHandler.TryCompleteMsg(msg);
            return trackingId;
        }

        private string ValidateInvoice(IBrokeredMessage msg, string document)
        {
            if (msg == null || string.IsNullOrEmpty(document))
                return string.Empty;
            _logger.Info($"{MessengerName}: {nameof(ValidateInvoice)} ");
            msg.RenewLock();
			const string UBFDOCTYPE = "UBF";
			var documentType = GetMsgPropertyValue(msg, DOCTYPE);
			if (string.IsNullOrEmpty(documentType))
			{				
				documentType = UBFDOCTYPE; // we defaulting old format(UBF) for backward compatibility
			}
			var producerId = GetMsgPropertyValue(msg, PRODUCERID);
			if (string.Compare(documentType, UBFDOCTYPE, true) == 0 && string.IsNullOrEmpty(producerId))
			{
				producerId = ReadProducerID(document);
				msg.Properties.Add(PRODUCERID, producerId);
			}
            var userName = GetMsgPropertyValue(msg, USERNAMEID);
			
			return _ebhWebService.PostValidationRequest(userName, producerId, document, documentType);
        }

        private string ReadProducerID(string ubf)
        {
            var producerId = "";
            using (var stringReader = new StringReader(ubf))
            {
                using (var xmlReader = new XmlTextReader(stringReader))
                {
                    var ubfNavigator = new XPathDocument(xmlReader).CreateNavigator();
                    producerId = ubfNavigator.SelectSingleNode("/UBFDocument/producer").GetAttribute("producerId", "");
                }
            }
            _logger.Info($"{MessengerName}: ProducerID: {producerId} ");
            return producerId;
        }
    }
}
