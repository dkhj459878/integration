﻿using Newtonsoft.Json;
using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using NextGen.Framework.Managers.StorageMgr;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Messengers
{
    using Enums;
    using Exceptions;
    using Extensions;
    using Helper;

    public class EbhServiceBusAttachmentReceiver : EbhServiceBusMessenger
    {
        protected const string ATTACHMENT = "attachment";
        protected const string WORKFLOWSTEP = "workflowstep";
        protected const string OPERATIONID = "operationid";
        protected const string ATTACHMENTID = "attachmentid";
        protected const string SUBMISSIONID = "submissionid";
        protected const string INVOICEID = "invoiceid";
        protected const string LOCATION = "location";
        protected const string ASSOCIATIONID = "associationid";

        public EbhServiceBusAttachmentReceiver(IServiceBusHandler sbHandler, EbhWebService ebhWebService, NxIOHelper ioHelper, ILogMgr logMgr)
            : base(sbHandler, ebhWebService, new NxIOHelper(logMgr, StorageName.Attachments), logMgr)
        { }

        public override string MessengerName => nameof(EbhServiceBusAttachmentReceiver);

        public override string ProcessMessage(IBrokeredMessage msg)
        {
            if (string.IsNullOrEmpty(base.ProcessMessage(msg)))
                return string.Empty;

            var step = GetMsgPropertyValue(msg, WORKFLOWSTEP);
            if (string.IsNullOrEmpty(step))
                return SendError(msg, "No workflow step specified");

            var userName = GetMsgPropertyValue(msg, USERNAMEID);
            if (string.IsNullOrEmpty(userName))
                return SendError(msg, "No user name specified");

            var producerId = GetMsgPropertyValue(msg, PRODUCERID);
            if (string.IsNullOrEmpty(producerId))
                return SendError(msg, "No producer ID specified");

            _logger.Info($"{MessengerName}: Message received. Workflow step: {step}; Producer ID: {producerId}; User name: {userName}.");

            var workflowStep = (AttachmentWorkflowSteps)Enum.Parse(typeof(AttachmentWorkflowSteps), step, true);
            switch (workflowStep)
            {
                case AttachmentWorkflowSteps.None:
                case AttachmentWorkflowSteps.Step1:
                    return UploadAttachment(msg, userName, producerId);
                case AttachmentWorkflowSteps.Step2:
                    return AddAttachment(msg, userName, producerId);
                case AttachmentWorkflowSteps.Step3:
                    return CommitSubmittedInvoice(msg, userName, producerId);
                default:
                    return string.Empty;
            }
        }

        public string UploadAttachment(IBrokeredMessage msg, string userName, string producerId)
        {
            if (!CheckParameters(msg))
                return string.Empty;

            var filePath = GetMsgPropertyValue(msg, LOADFILEPATHID);
            if (string.IsNullOrEmpty(filePath))
                return SendError(msg, "No attachment file path specified");

            _logger.Info($"{MessengerName}: Starting upload of file: '{filePath}'.");

            string responseData = null;
            string operationId = null;
            HttpStatusCode statusCode = HttpStatusCode.Continue;
            using (Stream stream = _ioHelper.ReadStream(filePath))
            {
                if (stream == null)
                    return SendError(msg, $"Could not get file stream. File: '{filePath}'");

                try
                {
                    var name = Path.GetFileName(filePath);
                    using (var response = _ebhWebService.PostAttachmentRequest(userName, producerId, name, stream))
                    {
                        responseData = _ebhWebService.ReadResponseStream(response, EbhApiContract.UploadAttachment);

                        if (response != null)
                        {
                            statusCode = response.StatusCode;
                            operationId = response.Headers.Get("OperationId");
                        }
                    }
                }
                catch (Exception e)
                {
                    return HandleException(msg, e);
                }
            }

            if (string.IsNullOrEmpty(responseData))
                return SendError(msg, "No response from EBH Attachment API");

            msg.RenewLock();
            var attachmentId = EbhWebService.ExtractField<string>(responseData, "ID");

            var fileName = SaveWorkflowStepResponse(msg, AttachmentWorkflowSteps.Step1, responseData);

            if (statusCode == HttpStatusCode.Continue)
                statusCode = EbhApiContract.UploadAttachment.GetSuccessfulApiStatusCode();

            if (!msg.Properties.ContainsKey(STATUSCODE))
                msg.Properties.Add(STATUSCODE, statusCode.ToString());

            var props = new Dictionary<string, object>
            {
                { RESPONSEFILEID, fileName },
                { STATUSCODE, statusCode.ToString() },
                { OPERATIONID, operationId },
                { ATTACHMENTID, attachmentId },
            };
            SendResponseMessage(msg, props);
            return fileName;
        }

        public string AddAttachment(IBrokeredMessage msg, string userName, string producerId)
        {
            if (!CheckParameters(msg))
                return string.Empty;

            var attachmentId = GetMsgPropertyValue(msg, ATTACHMENTID);
            if (string.IsNullOrEmpty(attachmentId))
                return SendError(msg, "No attachment ID specified");

            string responseData = string.Empty, location = string.Empty, operationId = string.Empty;
            HttpStatusCode statusCode = HttpStatusCode.Continue;
            try
            {
                var parameters = new
                {
                    AttachmentId = attachmentId,
                    // Invoice attachments optional parameters
                    //AssociatedElementId = invoiceId,
                    //AssociatedElementType = "Invoice"
                };
                string body = JsonConvert.SerializeObject(parameters);

                using (var response = _ebhWebService.PostAddAttachmentRequest(userName, producerId, msg.Get(SUBMISSIONID), msg.Get(INVOICEID), body))
                {
                    responseData = _ebhWebService.ReadResponseStream(response, EbhApiContract.AddAttachment);
                    if (response != null)
                    {
                        statusCode = response.StatusCode;
                        location = response.Headers.Get("Location");
                        operationId = response.Headers.Get("OperationId");
                    }
                }
            }
            catch (Exception e)
            {
                return HandleException(msg, e);
            }

            if (string.IsNullOrEmpty(responseData))
                return SendError(msg, "No response from EBH Attachment API");

            msg.RenewLock();
            var associationId = EbhWebService.ExtractField<string>(responseData, ID);

            var fileName = SaveWorkflowStepResponse(msg, AttachmentWorkflowSteps.Step2, responseData);

            if (statusCode == HttpStatusCode.Continue)
                statusCode = EbhApiContract.AddAttachment.GetSuccessfulApiStatusCode();

            if (!msg.Properties.ContainsKey(STATUSCODE))
                msg.Properties.Add(STATUSCODE, statusCode.ToString());

            var props = new Dictionary<string, object>
            {
                { RESPONSEFILEID, fileName },
                { STATUSCODE, statusCode.ToString() },
                { OPERATIONID, operationId },
                { ATTACHMENTID, attachmentId },
                { LOCATION, location },
                { ASSOCIATIONID, associationId },
            };
            SendResponseMessage(msg, props);
            return fileName;
        }

        public string CommitSubmittedInvoice(IBrokeredMessage msg, string userName, string producerId)
        {
            if (!CheckParameters(msg, true))
                return string.Empty;

            HttpStatusCode statusCode = HttpStatusCode.Continue;
            string operationId = PutCommitSubmittedInvoiceRequest(msg, userName, producerId, ref statusCode);

            var data = new { OperationId = operationId };
            var responseData = JsonConvert.SerializeObject(data);

            msg.RenewLock();
            var fileName = SaveWorkflowStepResponse(msg, AttachmentWorkflowSteps.Step3, responseData);

            SendCommitSubmittedInvoiceMessage(msg, statusCode, fileName, operationId);
            return fileName;
        }

        private string SaveWorkflowStepResponse(IBrokeredMessage msg, AttachmentWorkflowSteps step, string responseData)
        {
            var fileName = $"Attachment_{msg.Get(TRACKINGID)}_{msg.Get(ID)}_{step}.json";
            var file = _ioHelper.WriteOutputFile(fileName, responseData);
            return file.Combine();
        }

        private void SendCommitSubmittedInvoiceMessage(IBrokeredMessage msg, HttpStatusCode statusCode, string fileName, string operationId)
        {
            if (statusCode == HttpStatusCode.Continue)
                statusCode = EbhApiContract.CommitInvoice.GetSuccessfulApiStatusCode();

            if (!msg.Properties.ContainsKey(STATUSCODE))
                msg.Properties.Add(STATUSCODE, statusCode.ToString());

            var props = new Dictionary<string, object>
            {
                { RESPONSEFILEID, fileName },
                { STATUSCODE, statusCode.ToString() },
                { OPERATIONID, operationId },
            };
            SendResponseMessage(msg, props);
        }

        private bool CheckParameters(IBrokeredMessage msg, bool skipAttachmentParams = false)
        {
            var submissionId = GetMsgPropertyValue(msg, SUBMISSIONID);
            if (string.IsNullOrEmpty(submissionId))
                return SendError(msg, "No submission ID specified") != string.Empty;

            var invoiceId = GetMsgPropertyValue(msg, INVOICEID);
            if (string.IsNullOrEmpty(invoiceId))
                return SendError(msg, "No invoice ID specified") != string.Empty;

            if (!skipAttachmentParams)
            {
                var id = GetMsgPropertyValue(msg, ID);
                if (string.IsNullOrEmpty(id))
                    return SendError(msg, "No attachment item ID specified") != string.Empty;

                var parentItemId = GetMsgPropertyValue(msg, TRACKINGID);
                if (string.IsNullOrEmpty(parentItemId))
                    return SendError(msg, "No attachment parent item ID specified") != string.Empty;
            }
            return true;
        }

        private string PutCommitSubmittedInvoiceRequest(IBrokeredMessage msg, string userName, string producerId, ref HttpStatusCode statusCode)
        {
            try
            {
                using (var response = _ebhWebService.PutCommitSubmittedInvoiceRequest(userName, producerId, msg.Get(SUBMISSIONID), msg.Get(INVOICEID)))
                {
                    var responseData = _ebhWebService.ReadResponseStream(response, EbhApiContract.CommitInvoice);
                    if (!string.IsNullOrEmpty(responseData))
                        return SendError(msg, "Response from EBH Commit Submitted Invoice API should be empty");

                    statusCode = response.StatusCode;
                    return response.Headers.Get("OperationId");
                }
            }
            catch (Exception e)
            {
                return HandleException(msg, e);
            }
        }

        private string HandleException(IBrokeredMessage msg, Exception ex)
        {
            if (ex is EBHWebResponseException)
            {
                var e = ex as EBHWebResponseException;
                if (!msg.Properties.ContainsKey(STATUSCODE))
                    msg.Properties.Add(STATUSCODE, e.StatusCode.ToString());
            }
            return SendError(msg, $"Web service request failed. Error: {ex.Message}");
        }

        private string SendError(IBrokeredMessage msg, string errorMessage)
        {
            SendResponseErrorMsg(msg, $"{MessengerName}: {errorMessage}.");
            _serviceBusHandler.TryCompleteMsg(msg);
            return string.Empty;
        }
    }
}
