﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Messengers
{
    using Enums;
    using Exceptions;
    using Extensions;
    using Helper;

    public class EbhServiceBusInvoiceStatusReceiver : EbhServiceBusMessenger
	{
		public EbhServiceBusInvoiceStatusReceiver(IServiceBusHandler sbh, EbhWebService ebhh, NxIOHelper ioh, ILogMgr log)
			: base(sbh, ebhh, ioh, log)
		{ }

		public override string MessengerName => nameof(EbhServiceBusInvoiceStatusReceiver);

        public override string ProcessMessage(IBrokeredMessage msg)
        {
			if (string.IsNullOrEmpty(base.ProcessMessage(msg)))
				return string.Empty;

			var producerID = GetMsgPropertyValue(msg, PRODUCERID);
			_logger.Info($"{MessengerName}: Message received, ProducerID: {producerID}.");
			if (string.IsNullOrEmpty(producerID))
			{
				//cannot continue, missing required data
				var errMsg = $"InvStat, Missing 'producerId'";
				SendInvStatResponseErrorMsg(msg, errMsg);
				_serviceBusHandler.TryCompleteMsg(msg);
				return string.Empty;
			}

			try
			{
				var statusAction = GetMsgPropertyValue(msg, STATUSACTION);
				if (string.Equals(statusAction, "GETINVSTATUS", StringComparison.InvariantCultureIgnoreCase))
				{
					SendInvStatGetRequest(msg, producerID);
				}
				else if (string.Equals(statusAction, "UPDATEINVSTATUS", StringComparison.InvariantCultureIgnoreCase))
				{
					SendInvStatUpdateRequest(msg, producerID);
				}
				else if (string.Equals(statusAction, "GETINVSTATUSHISTORY", StringComparison.InvariantCultureIgnoreCase))
				{
					SendInvStatGetHistoryRequest(msg, producerID);
				}
			}
			catch (EBHWebResponseException ex)
			{
				SendInvStatWebResponseErrorMsg(msg, ex.Message, ex.StatusCode);
				throw;
			}
			catch (Exception ex)
			{
				SendInvStatResponseErrorMsg(msg, ex.Message);
				throw;
			}
			finally
			{
				_serviceBusHandler.TryCompleteMsg(msg);
			}
			return string.Empty;
		}

		private void SendInvStatUpdateRequest(IBrokeredMessage msg, string producerId)
		{
			var parameters = GetQueryParameters(msg);
			_logger.Info($"{MessengerName}: Message received, Parameters: {string.Join(", ", parameters)}.");
			var userName = GetMsgPropertyValue(msg, USERNAMEID);
			var body = GetMsgPropertyValue(msg, STATUSUPDATEBODY);
			var response = _ebhWebService.PutInvoiceStatusRequest(userName, producerId, parameters, body);
			var statusCode = EbhApiContract.InvStatusUpdate.GetSuccessfulApiStatusCode();

			SendInvStatResponseMsg(msg, statusCode);
		}

		private void SendInvStatGetHistoryRequest(IBrokeredMessage msg, string producerId)
		{
			var parameters = GetQueryParameters(msg);
			_logger.Info($"{MessengerName}: Message received, Parameters: {string.Join(", ", parameters)}.");

			var userName = GetMsgPropertyValue(msg, USERNAMEID);
			var response = _ebhWebService.GetInvoiceStatusHistory(userName, producerId, parameters);
			if (!string.IsNullOrEmpty(response))
			{
				var statusCode = EbhApiContract.InvStatusGetHistory.GetSuccessfulApiStatusCode();
				SendInvStatResponseMsg(msg, response, statusCode);
			}
			else
			{
				SendInvStatResponseErrorMsg(msg, $"{MessengerName}: No response from EBH InvoiceStatus."); //failed
			}
		}

		private void SendInvStatGetRequest(IBrokeredMessage msg, string producerId)
		{
			var filePath = GetMsgPropertyValue(msg, LOADFILEPATHID);
			_logger.Info($"{MessengerName}: Message received, File: '{filePath}'");
			var document = _ioHelper.ReadLoadFile(filePath);
			if (string.IsNullOrEmpty(document))
			{
				SendInvStatResponseErrorMsg(msg, $"{MessengerName}: Could not read file: '{filePath}'."); //failed
				return;
			}

			var userName = GetMsgPropertyValue(msg, USERNAMEID);
			var response = _ebhWebService.PostInvoiceStatusRequest(userName, producerId, document);
			if(!string.IsNullOrEmpty(response))
			{
				var statusCode = EbhApiContract.InvStatusGet.GetSuccessfulApiStatusCode();
				SendInvStatResponseMsg(msg, response, statusCode);
			}
			else
			{
				SendInvStatResponseErrorMsg(msg, $"{MessengerName}: No response from EBH InvoiceStatus."); //failed
			}
		}

		// GET
		private void SendInvStatResponseMsg(IBrokeredMessage curMsg, string flatFile, HttpStatusCode statusCode)
		{
			if (curMsg == null)
				return;
			curMsg.RenewLock();
			var producerID = GetMsgPropertyValue(curMsg, PRODUCERID);
			//write output file
			var fileName = $"InvStatResponse_{producerID}_{Guid.NewGuid()}.json";
			var pi = _ioHelper.WriteOutputFile(fileName, flatFile);
			//send result to Response
			fileName = pi.Combine();
			var props = new Dictionary<string, object>
			{
				{ RESPONSEFILEID, fileName },
				{ STATUSCODE, statusCode.ToString() },
			};
			SendResponseMessage(curMsg, props);
		}

		protected void SendInvStatResponseErrorMsg(IBrokeredMessage curMsg, string errorMsg)
		{
			if (curMsg == null)
				return;
			var pmsg = InitBrMsgWithErrorMsg(curMsg, errorMsg);

			_serviceBusHandler.SendMessage(pmsg);
		}

		protected void SendInvStatWebResponseErrorMsg(IBrokeredMessage curMsg, string errorMsg, HttpStatusCode statusCode)
		{
			if (curMsg == null)
				return;
			var pmsg = InitBrMsgWithErrorMsg(curMsg, errorMsg);
			pmsg.Properties.Add(STATUSCODE, statusCode.ToString());

			_serviceBusHandler.SendMessage(pmsg);
		}

		// PUT
		private void SendInvStatResponseMsg(IBrokeredMessage curMsg, HttpStatusCode statusCode)
		{
			if (curMsg == null)
				return;
			curMsg.RenewLock();
			//send result to Response
			var pmsg = _serviceBusHandler.CreateBrokeredMessage(curMsg);
			pmsg.Properties.Add(STATUSCODE, statusCode.ToString());

			_serviceBusHandler.SendMessage(pmsg);
		}

		private Dictionary<string, object> GetQueryParameters(IBrokeredMessage curMsg)
		{ 
			return curMsg.Properties.Where(prop => Enum.GetNames(typeof(InvStatusParameter))
				.Contains(prop.Key, StringComparer.InvariantCultureIgnoreCase)).ToDictionary(pair => pair.Key, pair => pair.Value);
		}
	}
}
