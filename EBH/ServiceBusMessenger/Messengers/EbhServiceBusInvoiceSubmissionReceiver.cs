﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Generic;
using System.Net;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Messengers
{
    using Enums;
    using Exceptions;
    using Extensions;
    using Helper;

    public class EbhServiceBusInvoiceSubmissionReceiver : EbhServiceBusMessenger
    {
        public EbhServiceBusInvoiceSubmissionReceiver(IServiceBusHandler sbh, EbhWebService ebhh, NxIOHelper ioh, ILogMgr log)
            : base(sbh, ebhh, ioh, log)
        { }

        public override string MessengerName => nameof(EbhServiceBusInvoiceSubmissionReceiver);

        public override string ProcessMessage(IBrokeredMessage msg)
        {
            if (string.IsNullOrEmpty(base.ProcessMessage(msg)))
                return string.Empty;

            var userName = GetMsgPropertyValue(msg, USERNAMEID);
            //Read the file
            var filePath = GetMsgPropertyValue(msg, LOADFILEPATHID);
            _logger.Info($"{MessengerName}: Message received, File: {filePath}");
            var document = _ioHelper.ReadLoadFile(filePath);
            if (string.IsNullOrEmpty(document))
            {
                //failed
                SendInvSubResponseErrorMsg(msg, $"{MessengerName}: Could not read file: '{filePath}'.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            var producerID = GetMsgPropertyValue(msg, PRODUCERID);
            _logger.Info($"{MessengerName}: Message received, ProducerID: {producerID}");
            if (string.IsNullOrEmpty(producerID))
            {
                SendInvSubResponseErrorMsg(msg, $"{MessengerName}: Missing 'producerId'.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            var documentType = GetMsgPropertyValue(msg, DOCTYPE);
            if (string.IsNullOrEmpty(documentType))
            {
                documentType = "ESF"; // we defaulting initial format(ESF) for invoice submission for backward compatibility
            }
            string response;
            try
            {
                response = _ebhWebService.PostInvoiceSubmissionRequest(userName, producerID, document, documentType);
            }
            catch (EBHWebResponseException ex)
            {
                SendInvWebSubResponseErrorMsg(msg, ex.Message, ex.StatusCode);
                throw;
            }
            catch (Exception ex)
            {
                SendInvSubResponseErrorMsg(msg, ex.Message);
                _serviceBusHandler.TryCompleteMsg(msg);
                throw;
            }

            if (string.IsNullOrEmpty(response))
            {
                //failed
                SendInvSubResponseErrorMsg(msg, $"{MessengerName}: No response from EBH InvoiceSubmission.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }

            var fileName = SendInvSubResponseMsg(msg, response);
            if (string.IsNullOrEmpty(fileName))
            {
                //failed
                SendInvSubResponseErrorMsg(msg, $"{MessengerName}: InvoiceSubmission Result File not created.");
                _serviceBusHandler.TryCompleteMsg(msg);
                return string.Empty;
            }
            _serviceBusHandler.TryCompleteMsg(msg);
            return fileName;
        }

        private string SendInvSubResponseMsg(IBrokeredMessage curMsg, string flatFile)
        {
            if (curMsg == null)
                return string.Empty;
            curMsg.RenewLock();
            //write output file
            var trackingId = EbhWebService.ExtractField<string>(flatFile, "ID");
            var fileName = $"InvSub_{trackingId}.json";
            var pi = _ioHelper.WriteOutputFile(fileName, flatFile);
            //send result to Response
            fileName = pi.Combine();
            var statusCode = EbhApiContract.Submission.GetSuccessfulApiStatusCode();

            var props = new Dictionary<string, object>
            {
                { RESPONSEFILEID, fileName },
                { STATUSCODE, statusCode.ToString() },
            };
            SendResponseMessage(curMsg, props);

            return fileName;
        }

        private void SendInvSubResponseErrorMsg(IBrokeredMessage curMsg, string errorMsg)
        {
            if (curMsg == null)
                return;
            var pmsg = InitBrMsgWithErrorMsg(curMsg, errorMsg);

            _serviceBusHandler.SendMessage(pmsg);
        }

        private void SendInvWebSubResponseErrorMsg(IBrokeredMessage curMsg, string errorMsg, HttpStatusCode statusCode)
        {
            if (curMsg == null)
                return;
            var pmsg = InitBrMsgWithErrorMsg(curMsg, errorMsg);
            pmsg.Properties.Add(STATUSCODE, statusCode.ToString());

            _serviceBusHandler.SendMessage(pmsg);
        }
    }
}
