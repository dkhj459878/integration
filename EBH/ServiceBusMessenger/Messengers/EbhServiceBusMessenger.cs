﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Messengers
{
    using Enums;
    using Exceptions;
    using Extensions;
    using Helper;

    public abstract class EbhServiceBusMessenger : IDisposable
    {
        protected const string ID = "id";
        protected const string TRACKINGID = "trackingid";
        protected const string LOADFILEPATHID = "loadfilepath";
        protected const string USERNAMEID = "username";
        protected const string PRODUCERID = "producerid";
		protected const string DOCTYPE = "documenttype";
		protected const string RESPONSEID = "response";
        protected const string RESPONSEFILEID = "responsefile";
        protected const string LASTSYNCDATEID = "lastsyncdate";
        protected const string CONFIGTYPE = "configtype";
        protected const int DEFTIMETOLIVE = 20;
        protected const string ERRORMSGID = "errormsg";
		protected const string STATUSCODE = "statuscode";
		protected const string STATUSACTION = "statusaction";
		protected const string STATUSUPDATEBODY = "statusupdatebody";

        protected readonly IServiceBusHandler _serviceBusHandler;
        protected readonly EbhWebService _ebhWebService;
        protected readonly NxIOHelper _ioHelper;
        protected readonly ILogMgr _logger;

        protected IQueueClient _queueClientPump;

        public enum RunStates
        {
            CONTINUE,
            WAIT,
            STOP
        }
        
        protected EbhServiceBusMessenger(IServiceBusHandler serviceBusHandler, EbhWebService ebhWebService, NxIOHelper ioHelper, ILogMgr logMgr)
        {
            _serviceBusHandler = serviceBusHandler ?? throw new ArgumentNullException("ServiceBusHandler cannot be null");
            _ebhWebService = ebhWebService ?? throw new ArgumentNullException("NxEBHWebServiceHandler cannot be null");
            _logger = logMgr ?? Globals.LogMgr ?? throw new ArgumentNullException($"{nameof(Globals.LogMgr)} cannot be null");
            _ioHelper = ioHelper ?? new NxIOHelper(_logger);
        }

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _queueClientPump.Close();

                    _queueClientPump = null;
                    LastError = null;
                }

                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~NxEBHSBMessengerBase()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        private readonly object _lock = new object();
        private RunStates _runState = RunStates.CONTINUE;

        public RunStates RunState
        {
            get => _runState;
            set
            {
                lock (_lock)
                {
                    _runState = value;
                    if (value != RunStates.CONTINUE && _queueClientPump != null)
                    {
                        _queueClientPump.Close();
                        _queueClientPump = null;
                    }
                }
            }
        }

        public Exception LastError { get; protected set; }

        public virtual void LogError(Exception ex)
        {
            _logger.Error(ex);
        }

        public virtual void LogError(string msg)
        {
            _logger.Error(msg);
        }

        public virtual string MessengerName => nameof(EbhServiceBusMessenger);

        public virtual void StartPump(int maxWorkers)
        {
            _logger.Info($"{MessengerName}: Pump started");
            _queueClientPump = _serviceBusHandler.ReceiveMessage(ProcessMessagePump, maxWorkers);
        }

        public virtual void ProcessMessagePump(IBrokeredMessage msg)
        {
            try
            {
                if (RunState == RunStates.CONTINUE)
                {
                    ProcessMessage(msg);
                }
            }
            catch (CriticalStopException ex)
            {
                LogError(ex);
                RunState = RunStates.STOP;
                LastError = ex;
            }
            catch (CriticalWaitException ex)
            {
                LogError(ex);
                RunState = RunStates.WAIT;
                LastError = ex;
            }
            catch (Exception ex)
            {
                LogError(ex);
                LastError = ex;
            }
        }

        protected virtual string HealthCheck(IBrokeredMessage message)
        {
            if (message == null)
                return null;

            var service = new EbhHealthCheckService(_logger, _ioHelper, _ebhWebService);
            var responseMessage = service.CreateResponseMessage(message, MessengerName);
            var statusCode = EbhApiContract.HealthCheck.GetSuccessfulApiStatusCode();
            var props = new Dictionary<string, object>
            {
                { EbhHealthCheckService.HEALTHCHECK, EbhHealthCheckService.HEALTHCHECK },
                { STATUSCODE, statusCode.ToString() },
            };
            SendResponseMessage(responseMessage, props);
            return string.Empty;
        }

        /// <summary>
        /// Processes message and checks health.
        /// </summary>
        /// <param name="msg">Brokered message</param>
        /// <returns>String of the result.</returns>
        public virtual string ProcessMessage(IBrokeredMessage msg)
        {
            if (msg == null)
                return null;

            if (msg.Properties.ContainsKey(EbhHealthCheckService.HEALTHCHECK))
                return HealthCheck(msg);

            return nameof(RunStates.CONTINUE);
        }

        public void SendResponseErrorMsg(IBrokeredMessage curMsg, string errorMsg)
        {
            if (curMsg == null)
                return;
            var errMsg = InitBrMsgWithErrorMsg(curMsg, errorMsg);
            _serviceBusHandler.SendMessage(errMsg);
        }

        protected virtual void SendResponseMessage(IBrokeredMessage message, Dictionary<string, object> extraProperties)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            if (extraProperties == null || extraProperties.Count == 0)
                throw new ArgumentException($"No extra properties!", nameof(extraProperties));

            var info = extraProperties.Select(x => $"{x.Key}: {x.Value};");
            _logger.Info($"{MessengerName}: Send response message. {string.Join(" ", info)}");

            message.RenewLock();
            var azureMsg = _serviceBusHandler.CreateBrokeredMessage(message, extraProperties);

            // Finally, send constructed response message
            _serviceBusHandler.SendMessage(azureMsg);
        }

        protected string GetMsgPropertyValue(IBrokeredMessage msg, string key)
        {
            if (msg.Properties.ContainsKey(key))
                return msg.Properties[key].ToString();
            return string.Empty;
        }

		protected IBrokeredMessage InitBrMsgWithErrorMsg(IBrokeredMessage curMsg, string errorMsg)
		{
			LogError(errorMsg);
			curMsg.RenewLock();
			var azureMsg = _serviceBusHandler.CreateBrokeredMessage(curMsg);

            if (azureMsg.Properties.ContainsKey(ERRORMSGID))
            {
                azureMsg.Properties[ERRORMSGID] = $"{errorMsg}. {azureMsg.Properties[ERRORMSGID]}";
            }
            else
            {
                azureMsg.Properties[ERRORMSGID] = errorMsg;
            }
			return azureMsg;
		}
    }
}
