﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using Storage.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger
{
    using Enums;
    using Helper;

    public class EbhHealthCheckService
	{
		public const string HEALTHCHECK = "HEALTHCHECK";
		private static readonly string serviceName = string.Join(" ", typeof(EbhHealthCheckService).FullName.Split('.').Reverse().Take(2).Reverse());
		protected ILogMgr _logger;
		protected NxIOHelper _ioHelper;
		protected EbhWebService _ebhHandler;

		public EbhHealthCheckService(ILogMgr logger, NxIOHelper ioHelper, EbhWebService ebhHandler)
		{
			_logger = logger;
			_ioHelper = ioHelper;
			_ebhHandler = ebhHandler;
		}

		public IBrokeredMessage CreateResponseMessage(IBrokeredMessage msg, string stageName)
		{
			_logger?.WriteLog($"{serviceName}: Message received, {stageName}", ILogMgr.SeverityLevel.Info);
            
            msg.Properties[HEALTHCHECK] 
                = $"{stageName} RequestResponse internal round trip;{nameof(EbhHealthCheckService)} version: {ResolveVersion()}";

            var isAdditionalCheckOptions = msg.Properties.ContainsKey(AdditionalCheckOptions.FileStorage.ToString()) || msg.Properties.ContainsKey(AdditionalCheckOptions.RestService.ToString());
			if (isAdditionalCheckOptions)
			{
				if (msg.Properties.ContainsKey(AdditionalCheckOptions.FileStorage.ToString()))
				{
					msg = AddAdditionalOptionResult(msg, stageName, AdditionalCheckOptions.FileStorage, CheckFileStorageStatus);
				}
				if (msg.Properties.ContainsKey(AdditionalCheckOptions.RestService.ToString()))
				{
					msg = AddAdditionalOptionResult(msg, stageName, AdditionalCheckOptions.RestService, CheckRestServiceStatus);
				}
			}

			return msg;
		}

		private string ResolveVersion()
		{
			string assemblyVersion;
			try
			{
				var assembly = Assembly.GetCallingAssembly();
				assemblyVersion = assembly.GetName().Version.ToString();
			}
			catch (Exception e)
			{
				_logger?.WriteLog(e, ILogMgr.SeverityLevel.Warning);
				assemblyVersion = $"{VersionInfo.ServiceVersion} (Rev.{VersionInfo.Revision})";
			}

			return assemblyVersion;
		}

		private IBrokeredMessage AddAdditionalOptionResult(IBrokeredMessage msg, string stageName, AdditionalCheckOptions option, Func<KeyValuePair<bool, string>> checkFunction)
		{
			msg.Properties.Remove(option.ToString());
			try
			{
				var resultStatus = checkFunction();
				msg.Properties.Add($"{stageName}:{option}", $"{resultStatus.Key};{resultStatus.Value}");
			}
			catch (Exception ex)
			{
				msg.Properties.Add($"{stageName}:{option}", $"{false};{ex.Message}");
			}
			
			
			return msg;
		}

		private KeyValuePair<bool, string> CheckRestServiceStatus()
		{	
			_logger?.WriteLog($"{serviceName}: eBH Rest Service Check", ILogMgr.SeverityLevel.Info);
			var result = _ebhHandler.HealthCheck(null);
			var message = $"{serviceName}: eBH Rest Service is available.";
			if (!result.Key)
			{
				message = $"{serviceName}: eBH Rest Service is unavailable. Response: {result.Value}";
			}
			_logger?.WriteLog(message, ILogMgr.SeverityLevel.Info);

			return result;
		}

		private KeyValuePair<bool, string> CheckFileStorageStatus()
		{
			_logger?.WriteLog($"{serviceName}: Write file to {_ioHelper.Storage.GetStorageUri()}", ILogMgr.SeverityLevel.Info);
			var timeStamp = DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss.fff");
			var storage = _ioHelper.Storage;
			var pathInfo = new PathInfo(HEALTHCHECK, $"{HEALTHCHECK}_{timeStamp}");
			var fullPath = Path.Combine(storage.GetFileUri(pathInfo));
			var message = $"File {fullPath} has been created successfully";

			var result = new KeyValuePair<bool, string>(true, $"{serviceName}: {message}");
			try
			{
				pathInfo = _ioHelper.WriteOutputFile(pathInfo.FileName, HEALTHCHECK, pathInfo.FolderName);
				var filePath = storage.GetFileUri(pathInfo);				

				if (!storage.FileExists(pathInfo))
				{
					result = new KeyValuePair<bool, string>(false, $"{serviceName}: Test file {fullPath} has not been created");
				}
				_logger?.WriteLog(message, ILogMgr.SeverityLevel.Info);
			}
			catch (Exception ex)
			{
				_logger?.WriteLog(ex.Message, ILogMgr.SeverityLevel.Error);
				var errorMessage = ex.Message + Environment.NewLine + $"File path:{fullPath}";
				result = new KeyValuePair<bool, string>(false, errorMessage);
				storage.DeleteFileIfExists(pathInfo);
				storage.DeleteFolderIfExists(pathInfo.FolderName);
			}

			return result;
		}
	}
}