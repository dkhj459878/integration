﻿using NextGen.Framework.Managers.ConfigurationMgr;
using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Generic;

namespace NextGen.Framework.Integration.EBH.ServiceBusService
{
    using Helper;

    /// <summary>
    /// On-Prem ServiceBus Handler
    /// </summary>
    public class NxServiceBusHandler : IServiceBusHandler
    {
        private const int DEFTIMETOLIVE = 20;
        protected static NxServiceBusMgr _sbMgr;
        static ILogMgr _logMgr;
        static NxServiceBusHandler()
        {
            _logMgr = Globals.LogMgr;
            _sbMgr = NxServiceBusMgr.Current;
            
            var configSection = NxConfigMgr.Current.GetConfigSection("nextGen/managers/globalManagers/ServiceBusMgr");
            if (configSection != null && configSection is NxConfigSection)
                NxServiceBusMgr.Initialize(configSection, _logMgr, NxConfigMgr.Current.ExternalConfigProvider?.TenantId);
        }
        public NxServiceBusHandler(Func<IQueueClient> requestQueueClientProvider, Func<IQueueClient> responseQueueClientProvider)
        {
            this.requestQueueClientProvider = requestQueueClientProvider;
            this.responseQueueClientProvider = responseQueueClientProvider;
        }

        #region QueueClients

        protected IQueueClient currentRequestQueueClient;
        protected IQueueClient currentResponseQueueClient;

        protected readonly Func<IQueueClient> requestQueueClientProvider;
        protected readonly Func<IQueueClient> responseQueueClientProvider;
       
        #endregion

        public IQueueClient ReceiveMessage(Action<IBrokeredMessage> callback, int maxWorkers)
        {
            var queueClient = ResolveRequestClient();
            queueClient.OnMessage(callback, maxWorkers);
            return queueClient;
        }

        protected IQueueClient ResolveRequestClient()
        {
            if (currentRequestQueueClient == null)
                currentRequestQueueClient = requestQueueClientProvider?.Invoke();
            return currentRequestQueueClient;
        }

        protected IQueueClient ResolveResponseClient()
        {
            if (currentResponseQueueClient == null)
                currentResponseQueueClient = responseQueueClientProvider?.Invoke();
            return currentResponseQueueClient;
        }

        public virtual void SendMessage(IBrokeredMessage msg)
        {
            ResolveResponseClient().Send(msg);
        }
      
        #region CompleteMessages
        
        public virtual bool TryCompleteMsg(IBrokeredMessage msg)
        {
            try
            {
                msg.Complete();
            }
            catch { return false; }
            return true;
        }
        #endregion

        public IBrokeredMessage CreateBrokeredMessage()
        {
            return new NxBrokeredMessage() { TimeToLive = new TimeSpan(0, DEFTIMETOLIVE, 0) };
        }

        public IBrokeredMessage CreateBrokeredMessage(IBrokeredMessage originalMsg, IDictionary<string, object> extraProps = null)
        {
            return new NxBrokeredMessage(originalMsg, extraProps) { TimeToLive = new TimeSpan(0, DEFTIMETOLIVE, 0) };
        }

        public bool IsConnected
        {
            get
            {
                return _sbMgr.IsConnected;
            }
        }
    }
}
