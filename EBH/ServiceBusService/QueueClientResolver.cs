﻿using NextGen.Framework.Managers.ServiceBusMgr;

namespace NextGen.Framework.Integration.EBH.ServiceBusService
{
    using ServiceBusMessenger.Queues;

    public class QueueClientResolver
    {
        private readonly IServiceBusMgr _serviceBusMgr;

        public QueueClientResolver(IServiceBusMgr serviceBusMgr)
        {
            _serviceBusMgr = serviceBusMgr;
        }

        public IQueueClient QCRequest()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhValidateRequest);
        }

        public IQueueClient QCPending()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhValidatePending);
        }

        public IQueueClient QCResponse()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhValidateResponse);
        }

        public IQueueClient QCConfigurationRequest()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhConfigurationRequest);
        }

        public IQueueClient QCConfigurationResponse()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhConfigurationResponse);
        }

        public IQueueClient QCInvStatRequest()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusRequest);
        }

        public IQueueClient QCInvSubRequest()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceSubmissionRequest);
        }

        public IQueueClient QCInvStatResponse()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceStatusResponse);
        }

        public IQueueClient QCInvSubResponse()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhInvoiceSubmissionResponse);
        }

        public IQueueClient QCAttachmentRequest()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhAttachmentRequest);
        }

        public IQueueClient QCAttachmentResponse()
        {
            return _serviceBusMgr.GetQueueClient(QueueNames.Namespace, QueueNames.EbhAttachmentResponse);
        }
    }
}