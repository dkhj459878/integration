﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Xml;

namespace NextGen.Framework.Integration.EBH.ServiceBusService
{
    [RunInstaller(true)]
    public partial class InstallerSetup : System.Configuration.Install.Installer
    {
        public InstallerSetup()
        {
            InitializeComponent();
        }

        public override void Install(IDictionary stateSaver)
        {            
            string assemblypath = Context.Parameters["assemblypath"];
            // This gets the named parameters passed in from your custom action
            string folder = Path.GetDirectoryName(assemblypath);

            // This gets the "Authenticated Users" group, no matter what it's called
            SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.NetworkServiceSid, null);

            // Create the rules
            FileSystemAccessRule writerule = new FileSystemAccessRule(sid, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);

            if (!string.IsNullOrEmpty(folder) && Directory.Exists(folder))
            {
                // Get your file's ACL
                DirectorySecurity fsecurity = Directory.GetAccessControl(folder);

                // Add the new rule to the ACL
                fsecurity.AddAccessRule(writerule);

                // Set the ACL back to the file
                Directory.SetAccessControl(folder, fsecurity);
            }

            // Explicitly call the overriden method to properly return control to the installer
            base.Install(stateSaver);
        }

        public override void Commit(IDictionary savedState)
        {            
            base.Commit(savedState);

            try
            {
                AddConfigurationFileDetails();
            }
            catch
            {                
                base.Rollback(savedState);
            }
        }

        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }

        public override void Uninstall(IDictionary savedState)
        {
            base.Uninstall(savedState);
        }

        private void AddConfigurationFileDetails()
        {
            
            string assemblypath = Context.Parameters["assemblypath"];
            string appConfigPath = assemblypath + ".config";
            var config = ConfigurationManager.OpenExeConfiguration(assemblypath);

            ModifyConfigParam(config, "RootPath", Path.GetDirectoryName(assemblypath));

            var installerParam = Context.Parameters["EBHURL"];
            if (!string.IsNullOrEmpty(installerParam))
            {
                ModifyConfigParam(config, "EBHUrl", installerParam);       
            }
              
            installerParam = Context.Parameters["PROVIDERKEY"];
            if (!string.IsNullOrEmpty(installerParam))
            {
                ModifyConfigParam(config, "ProviderKey", installerParam);
            }

            config.Save(ConfigurationSaveMode.Modified);

            ConfigurationManager.RefreshSection("appSettings");
                
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(appConfigPath);
            installerParam = Context.Parameters["APPLICATIONOUTPUTPATH"];
            if (!string.IsNullOrEmpty(installerParam))
            {
                xmlDoc.SelectSingleNode("//nextGen/managers/globalManagers/printManager/renderService/applicatonOutputPath").Attributes["value"].Value = installerParam;
            }
            installerParam = Context.Parameters["SERVICEBUSHOST"];
            if (!string.IsNullOrEmpty(installerParam))
            {
                xmlDoc.SelectSingleNode("//nextGen/managers/globalManagers/ServiceBusMgr").Attributes["host"].Value = installerParam;
            }
            xmlDoc.Save(appConfigPath);
            
        }

        private void ModifyConfigParam(Configuration config, string configParamName, string newParamValue)
        {
            KeyValueConfigurationElement ebhUrl = config.AppSettings.Settings[configParamName];
            if (ebhUrl != null)
                ebhUrl.Value = newParamValue;
        }
    }

}
