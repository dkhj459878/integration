﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Threading;
using System.Threading.Tasks;
using Topshelf;

namespace NextGen.Framework.Integration.EBH.ServiceBusService
{
    using Helper;
    using ServiceBusMessenger;
    using ServiceBusMessenger.Messengers;

    public class ServiceBusService
    {
        public enum Handles
        {
            ALL,
            REQUESTONLY,
            PENDINGONLY,
            CONFIGONLY,
            INVSUBONLY,
            INVSTATONLY
        }
        private static bool _continue = true;
        private static int _maxWorkers = Program.DEFMAXWORKERS; //Max # of Messenger objects at a time(per queue)
        private static Handles _mode = Handles.ALL;
        private HostControl _hc;
        private Task _current;
        private EbhServiceBusHealthCheck _healthCheck;
        private ILogMgr _logMgr;
        private EbhServiceBusValidationReceiver validationReceiver;
        private EbhServiceBusValidationResultReciever validationResultReciever;
        private EbhServiceBusConfigurationReceiver configurationReceiver;
        private EbhServiceBusInvoiceSubmissionReceiver invoiceSubmissionReceiver;
        private EbhServiceBusInvoiceStatusReceiver invoiceStatusReceiver;
        private EbhServiceBusAttachmentReceiver attachmentReceiver;

        public ServiceBusService()
        {
            _logMgr = Globals.LogMgr ?? throw new NullReferenceException(nameof(Globals.LogMgr));
        }

        public int MaxWorkers
        {
            set { _maxWorkers = value; }
        }

        public Handles Mode
        {
            set { _mode = value; }
        }

        public HostControl HostCtl
        {
            set { _hc = value; }
        }

        public bool Start()
        {
            _continue = true;
            var version = $"{ VersionInfo.ServiceVersion } (Rev.{VersionInfo.Revision })";
            _logMgr.Info($"Initializing {nameof(ServiceBusService)} {version}");
            _current = new Task(Runner);
            _logMgr.Info($"Starting {nameof(ServiceBusService)} {version}");
            _current.Start();
            return true;
        }

        private void Runner()
        {
            try
            {
                var sbMgr = NxServiceBusMgr.Current;
                var queueClientResolver = new QueueClientResolver(sbMgr);
                var webService = new EbhWebService(new EbhWebServiceConfiguration(), _logMgr);
                _healthCheck = new EbhServiceBusHealthCheck(new NxServiceBusHandler(null, null), webService);

                validationReceiver = new EbhServiceBusValidationReceiver(new NxServiceBusHandler(queueClientResolver.QCRequest, queueClientResolver.QCPending), webService, null, _logMgr);
                validationResultReciever = new EbhServiceBusValidationResultReciever(new NxServiceBusHandler(queueClientResolver.QCPending, queueClientResolver.QCResponse), webService, null, _logMgr);
                configurationReceiver = new EbhServiceBusConfigurationReceiver(new NxServiceBusHandler(queueClientResolver.QCConfigurationRequest, queueClientResolver.QCConfigurationResponse), webService, null, _logMgr);
                invoiceSubmissionReceiver = new EbhServiceBusInvoiceSubmissionReceiver(new NxServiceBusHandler(queueClientResolver.QCInvSubRequest, queueClientResolver.QCInvSubResponse), webService, null, _logMgr);
                invoiceStatusReceiver = new EbhServiceBusInvoiceStatusReceiver(new NxServiceBusHandler(queueClientResolver.QCInvStatRequest, queueClientResolver.QCInvStatResponse), webService, null, _logMgr);
                attachmentReceiver = new EbhServiceBusAttachmentReceiver(new NxServiceBusHandler(queueClientResolver.QCAttachmentRequest, queueClientResolver.QCAttachmentResponse), webService, null, _logMgr);

                if (!_healthCheck.SBHealthCheck())
                {
                    _logMgr.Error("Service Bus failed HealthCheck");
                    _hc.Stop();
                    return;
                }

                // Start Receiver
                if (_mode == Handles.REQUESTONLY || _mode == Handles.ALL)
                    validationReceiver.StartPump(_maxWorkers);
                // Start Sender
                if (_mode == Handles.PENDINGONLY || _mode == Handles.ALL)
                    validationResultReciever.StartPump(_maxWorkers);
                // Start Config
                if (_mode == Handles.CONFIGONLY || _mode == Handles.ALL)
                    configurationReceiver.StartPump(_maxWorkers);
                // Start Invoice Submission
                if (_mode == Handles.CONFIGONLY || _mode == Handles.ALL)
                    invoiceSubmissionReceiver.StartPump(_maxWorkers);
                // Start Invoice Status
                if (_mode == Handles.CONFIGONLY || _mode == Handles.ALL)
                    invoiceStatusReceiver.StartPump(_maxWorkers);
                // Start Attachment
                if (_mode == Handles.CONFIGONLY || _mode == Handles.ALL)
                    attachmentReceiver.StartPump(_maxWorkers);
            }
            catch (Exception ex)
            {
                _logMgr.Error($@"Critical error during initialization. Message: {ex.Message}
Stack trace: {ex.StackTrace}.
Inner: {ex.InnerException?.Message}");
                _continue = false;
                _hc.Stop();
                return;
            }

            // Stop on errors
            while (_continue)
            {
                EbhServiceBusMessenger.RunStates curState = EbhServiceBusMessenger.RunStates.CONTINUE;
                if (validationReceiver.RunState != EbhServiceBusMessenger.RunStates.CONTINUE)
                    curState = validationReceiver.RunState;
                if (curState == EbhServiceBusMessenger.RunStates.CONTINUE && validationResultReciever.RunState != EbhServiceBusMessenger.RunStates.CONTINUE)
                    curState = validationResultReciever.RunState;
                if (curState == EbhServiceBusMessenger.RunStates.CONTINUE && configurationReceiver.RunState != EbhServiceBusMessenger.RunStates.CONTINUE)
                    curState = configurationReceiver.RunState;
                if (curState == EbhServiceBusMessenger.RunStates.CONTINUE && invoiceSubmissionReceiver.RunState != EbhServiceBusMessenger.RunStates.CONTINUE)
                    curState = invoiceSubmissionReceiver.RunState;
                if (curState == EbhServiceBusMessenger.RunStates.CONTINUE && invoiceStatusReceiver.RunState != EbhServiceBusMessenger.RunStates.CONTINUE)
                    curState = invoiceStatusReceiver.RunState;
                if (curState == EbhServiceBusMessenger.RunStates.CONTINUE && configurationReceiver.RunState != EbhServiceBusMessenger.RunStates.CONTINUE)
                    curState = attachmentReceiver.RunState;

                if (curState != EbhServiceBusMessenger.RunStates.CONTINUE)
                {
                    _logMgr.Info("Critical fault, stopping all pumps");
                    validationReceiver.RunState = curState;
                    validationResultReciever.RunState = curState;
                    configurationReceiver.RunState = curState;
                    invoiceSubmissionReceiver.RunState = curState;
                    invoiceStatusReceiver.RunState = curState;
                    attachmentReceiver.RunState = curState;

                    _continue = false;

                    if (curState == EbhServiceBusMessenger.RunStates.WAIT)
                        StartHealthCheck();
                    else if (curState == EbhServiceBusMessenger.RunStates.STOP)
                        _hc.Stop();
                }
                Thread.Sleep(TimeSpan.FromMilliseconds(100));
            }
        }

        public bool Stop()
        {
            _logMgr.Info($"Stopping {nameof(ServiceBusService)}");
            _continue = false;
            if (_current != null && !_current.IsCompleted)
            {
                _current.Wait(5000);
                _current = null;
            }
            return true;
        }

        public void StartHealthCheck()
        {
            _logMgr.Info("Starting EBH Service HealthCheck");
            var t = new Task(() =>
            {
                _healthCheck.EBHHealthCheckLoop();
                _logMgr.Info("HealthCheck complete");
                Start();
            });
            t.Start();
        }
    }
}
