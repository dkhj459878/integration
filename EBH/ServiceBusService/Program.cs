﻿using System;
using Topshelf;

namespace NextGen.Framework.Integration.EBH.ServiceBusService
{
    using Helper;

    class Program
    {
        public const int DEFMAXWORKERS = 20;
        static void Main(string[] args)
        {
            Globals.InitForOnPrem();
            //command line options example: -maxworkers:30 -mode:requestonly
            var h = HostFactory.Run(hostSettings =>
            {
                var appSettings = System.Configuration.ConfigurationManager.AppSettings;
                ServiceBusService.Handles mode = ParseMode(appSettings["Mode"]);
                int maxworkers = ParseMaxWorkers(appSettings["MaxWorkers"]);                
                hostSettings.Service<ServiceBusService>(serviceSettings =>
                {
                    serviceSettings.ConstructUsing(() => new ServiceBusService() { MaxWorkers = maxworkers, Mode = mode });
                    serviceSettings.WhenStarted((service, control) => { service.HostCtl = control; return service.Start(); });
                    serviceSettings.WhenStopped((service, control) => { return service.Stop(); });
                    serviceSettings.WhenPaused((service, control) => { return service.Stop(); });
                    serviceSettings.WhenContinued((service, control) => { return service.Start(); });
                });
                hostSettings.RunAsPrompt();                
                //note: command line only works when starting from command prompt; from ms services, you must use config file instead
                hostSettings.AddCommandLineDefinition("maxworkers", v => maxworkers = ParseMaxWorkers(v));
                hostSettings.AddCommandLineDefinition("mode", v => mode = ParseMode(v));

                hostSettings.SetServiceName("EBHSBService");
                hostSettings.SetDescription("3E Service Bus to EBH service");                
            });            
        }

        private static int ParseMaxWorkers(string v)
        {
            int maxworkers;
            int.TryParse(v, out maxworkers);
            if (maxworkers == 0)
                maxworkers = DEFMAXWORKERS;
            return maxworkers;
        }

        private static ServiceBusService.Handles ParseMode(string v)
        {
            ServiceBusService.Handles mode;
            Enum.TryParse<ServiceBusService.Handles>(v, true, out mode);
            return mode;
        }
    }
}
