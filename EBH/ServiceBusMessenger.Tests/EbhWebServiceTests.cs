﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NextGen.Framework.Managers.LogMgr;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Tests
{
    [TestClass]
    public class EbhWebServiceTests
    {
        private readonly MockRepository mockRepository;
        private readonly Mock<IEbhWebServiceConfiguration> ebhWebServiceConfiguration;
        private readonly Mock<ILogMgr> logMgr;
        private EbhWebService service;

        public EbhWebServiceTests()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);
            ebhWebServiceConfiguration = mockRepository.Create<IEbhWebServiceConfiguration>();
            ebhWebServiceConfiguration.SetupGet(x => x.Url).Returns("https://ebillinghub.azure-api.net");
            ebhWebServiceConfiguration.SetupGet(x => x.ProviderKey).Returns("");
            ebhWebServiceConfiguration.SetupGet(x => x.RequestTimeout).Returns("120000");

            logMgr = mockRepository.Create<ILogMgr>();

            service = new EbhWebService(ebhWebServiceConfiguration.Object, logMgr.Object);
        }

        public class IdObject
        {
            public string Id { get; set; }
        }

        [TestMethod, TestCategory("Connected")]
        public void PostAttachmentRequest_HappyPath_ReturnsResponseFromEBH()
        {
            // Arrange
            var fileContent = "0123456789";
            var buffer = Encoding.UTF8.GetBytes(fileContent);
            Stream stream = new MemoryStream(buffer);

            logMgr.Setup(l => l.Info(It.IsAny<string>(), It.IsAny<string[]>()))
                .Verifiable();

            var connectedService = new EbhWebService(new EbhWebServiceConfiguration(), logMgr.Object);
            var postResponse = string.Empty;

            // Act
            using (var response = connectedService.PostAttachmentRequest("username", "405", $"{nameof(PostAttachmentRequest_HappyPath_ReturnsResponseFromEBH)}.txt", stream))
            {
                postResponse = connectedService.ReadResponseStream(response, Enums.EbhApiContract.UploadAttachment);
            }

            // Assert
            logMgr.Verify(
                l => l.Info(It.IsAny<string>(), It.IsAny<string[]>()),
                Times.Exactly(4));
            Assert.IsFalse(string.IsNullOrEmpty(postResponse));
            Assert.IsTrue(postResponse.Length > 1);
            var obj = JsonConvert.DeserializeObject<IdObject>(postResponse);
            Assert.IsNotNull(obj);
            Assert.IsFalse(string.IsNullOrEmpty(obj.Id));
            var isValidId = Guid.TryParse(obj.Id, out Guid attachmentId);
            Assert.IsTrue(isValidId);
            Assert.AreNotEqual(Guid.Empty, attachmentId);
        }

        [Ignore, TestMethod, TestCategory("ToDo")]
        public void PostAddAttachmentRequest_HappyPath_ReturnsResponseFromEBH()
        {
            // Arrange
            var submissionId = "bf884799-df46-4a34-92e1-fde0df061c69";
            var invoiceId = "11926";
            var attachmentId = "8d298305-0f5a-4e07-0e02-08d85a2a4b5a";
            var requestBody = $"{{\"AttachmentId\":\"{attachmentId}\"}}";

            logMgr.Setup(l => l.Info(It.IsAny<string>(), It.IsAny<string[]>()))
                .Verifiable();

            var connectedService = new EbhWebService(new EbhWebServiceConfiguration(), logMgr.Object);
            var postResponse = string.Empty;

            // Act
            using (var response = connectedService.PostAddAttachmentRequest("TEN\\UC263303", "714", submissionId, invoiceId, requestBody))
            {
                postResponse = connectedService.ReadResponseStream(response, Enums.EbhApiContract.AddAttachment);
            }

            // Assert
            logMgr.Verify(
                l => l.WriteLog(It.IsAny<string>(), It.IsAny<ILogMgr.SeverityLevel>(), It.IsAny<string[]>()),
                Times.Exactly(2));
            logMgr.Verify(
                l => l.Info(It.IsAny<string>(), It.IsAny<string[]>()),
                Times.Exactly(2));
            Assert.IsFalse(string.IsNullOrEmpty(postResponse));
            Assert.IsTrue(postResponse.Length > 1);
        }
    }
}
