﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NextGen.Framework.Integration.EBH.Helper;
using NextGen.Framework.Integration.EBH.ServiceBusMessenger.Exceptions;
using NextGen.Framework.Integration.EBH.ServiceBusMessenger.Messengers;
using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using Rhino.Mocks;
using Storage.Interfaces;
using System;

namespace NextGen.Framework.Integration.EBH.ServiceBusMessenger.Tests
{
    [TestClass]
    public class ServiceBusMessengerTests
    {
        private readonly ILogMgr log;

        public ServiceBusMessengerTests()
        {
            log = MockRepository.GenerateStub<ILogMgr>();
            log.Stub(x => x.ApplicationListeners).Return(new NxLogListenerCollection());
            Globals.InitForOnPrem(log);
        }

        [TestMethod]
        public void ReceiverTest()
        {
            // Arrange
            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Stub(h => h.RenewLock()).Return(true);
            msg.Properties.Add("username", "user");
            msg.Properties.Add("loadfilepath", "xyz.xml");

            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            ebhh.Stub(h => h.PostValidationRequest(null, null, null, null)).IgnoreArguments().Return("1111");

            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();
            ioh.Stub(h => h.ReadLoadFile(null)).IgnoreArguments().Return("<UBFDocument><producer producerId = \"405\"></producer></UBFDocument>");

            // Act
            var rec = new EbhServiceBusValidationReceiver(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.AreEqual("1111", result);
            log.AssertWasNotCalled(
                x => x.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Times(4));
        }

        [TestMethod]
        public void SenderTest()
        {
            // Arrange
            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Properties.Add("username", "user");
            msg.Properties.Add("trackingid", "1111");
            msg.Properties.Add("producerid", "405");
            msg.Stub(h => h.RenewLock()).Return(true);

            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            ebhh.Stub(h => h.GetValidationResultByTrackingId(null, null, null)).IgnoreArguments().Return("some validated results");

            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();
            ioh.Stub(h => h.WriteOutputFile(null, null)).IgnoreArguments().Return(new PathInfo("test", "test.xml"));

            // Act
            var rec = new EbhServiceBusValidationResultReciever(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.AreEqual("test\\test.xml", result);
            log.AssertWasNotCalled(
                x => x.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Twice());
        }

        [TestMethod]
        public void ReceiverFailNoMsgTest()
        {
            // Arrange
            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();

            // Act
            var rec = new EbhServiceBusValidationReceiver(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(null);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(
                h => h.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasNotCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything));
        }

        [TestMethod]
        public void ReceiverFailEmptyFileTest()
        {
            // Arrange
            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();

            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Stub(h => h.RenewLock()).Return(true);
            msg.Properties.Add("username", "user");
            msg.Properties.Add("loadfilepath", "xyz.xml");
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());
            ioh.Stub(h => h.ReadLoadFile(null)).IgnoreArguments().Return(""); // Empty Load file

            // Act
            var rec = new EbhServiceBusValidationReceiver(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(
                h => h.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Once());
        }

        [TestMethod]
        public void ReceiverFailNoTrackIdTest()
        {
            // Arrange
            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();

            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Stub(h => h.RenewLock()).Return(true);
            msg.Properties.Add("username", "user");
            msg.Properties.Add("loadfilepath", "xyz.xml");
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());
            ioh.Stub(h => h.ReadLoadFile(null)).IgnoreArguments().Return("<UBFDocument><producer producerId = \"405\"></producer></UBFDocument>");
            ebhh.Stub(h => h.PostValidationRequest(null, null, null, null)).IgnoreArguments().Return("");

            // Act
            var rec = new EbhServiceBusValidationReceiver(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(
                x => x.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Times(3));
        }

        [TestMethod]
        public void SenderFailNoMsgTest()
        {
            // Arrange
            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();

            // Act
            var rec = new EbhServiceBusValidationResultReciever(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(null);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(
                x => x.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasNotCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything));
        }

        [TestMethod]
        public void SenderFailEmptyResponseTest()
        {
            // Arrange
            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Properties.Add("username", "user");
            msg.Properties.Add("trackingid", "1111");
            msg.Properties.Add("producerid", "405");
            msg.Stub(h => h.RenewLock()).Return(true);

            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            ebhh.Stub(h => h.GetValidationResultByTrackingId(null, null, null)).IgnoreArguments().Return("");

            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();

            // Act
            var rec = new EbhServiceBusValidationResultReciever(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(
                h => h.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Once());
        }


        /// <summary>
        /// Make actual call to EBH (non-mock)
        /// </summary>
        [TestMethod]
        public void ConfigTest_NoMockEBH()
        {
            // Arrange
            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Properties.Add("username", "user");
            msg.Properties.Add("producerid", "438");
            msg.Properties.Add("lastsyncdate", DateTime.UtcNow);
            msg.Properties.Add("configtype", EbhWebService.GETCLIENTS);
            msg.Stub(h => h.RenewLock()).Return(true);

            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();

            // Act
            var ebhh = new EbhWebService(new EbhWebServiceConfiguration(), log); // use real (non-mock)object
            var cfg = new EbhServiceBusConfigurationReceiver(sbh, ebhh, ioh, log);
            var result = cfg.ProcessMessage(msg);

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(result));
            Assert.IsTrue(ioh.Storage.FileExists(new PathInfo(result)));
            log.AssertWasNotCalled(
                x => x.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Times(18));
        }

        /// <summary>
        /// Make actual call to EBH(non-mock). No date
        /// </summary>
        [TestMethod]
        [TestCategory("Connected")]
        public void ConfigTest_NoMockEBH_NoDate()
        {
            // Arrange
            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Properties.Add("username", "user");
            msg.Properties.Add("producerid", "438");
            //msg.Properties.Add("lastsyncdate", null) // no date
            msg.Properties.Add("configtype", EbhWebService.GETCLIENTS);
            msg.Stub(h => h.RenewLock()).Return(true);

            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();
            var ebhh = new EbhWebService(new EbhWebServiceConfiguration(), log);//use real (non-mock)object

            // Act
            var cfg = new EbhServiceBusConfigurationReceiver(sbh, ebhh, ioh, log);
            var result = cfg.ProcessMessage(msg);

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(result));
            Assert.IsTrue(ioh.Storage.FileExists(new PathInfo(result)));
            log.AssertWasNotCalled(
                x => x.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Times(5));
        }

        [TestMethod]
        [TestCategory("Connected")]
        public void ReceiverNonCriticalFailTest()
        {
            // Arrange
            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Stub(h => h.RenewLock()).Return(true);
            msg.Properties.Add("username", "user");
            msg.Properties.Add("loadfilepath", "xyz.xml");

            NxBrokeredMessage actualMsg = null;
            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(Arg<IBrokeredMessage>.Is.Anything))
                .WhenCalled(method =>
                {
                    actualMsg = (NxBrokeredMessage)method.Arguments[0];
                });
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            //ebhh.Stub(h => h.POSTValidationRequest(null, null, null)).IgnoreArguments().Throw(new WebException() {  });

            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();
            ioh.Stub(h => h.ReadLoadFile(null)).IgnoreArguments().Return("<UBFDocument><producer producerId = \"438\"></producer></UBFDocument>");

            // Act
            var rec = new EbhServiceBusValidationReceiver(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            sbh.AssertWasCalled(
                x => x.SendMessage(Arg<IBrokeredMessage>.Is.Anything),
                o => o.Repeat.Once());
            Assert.IsTrue(actualMsg.Properties.ContainsKey("errormsg"));
            log.AssertWasNotCalled(
                x => x.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Times(7));
        }

        /// <summary>
        /// Tests cases with throwing of the <see cref="CriticalStopException"></see> objects.
        /// </summary>
        [TestMethod]
        [TestCategory("Connected")]
        public void NxEBHSBReceiver_ProcessValidateReqMsg_ThrowsCriticalStopException()
        {
            // Arrange
            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Stub(h => h.RenewLock()).Return(true);
            msg.Properties.Add("username", "user");
            msg.Properties.Add("loadfilepath", "xyz.xml");

            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            var config = MockRepository.GenerateStub<IEbhWebServiceConfiguration>();
            var realConfig = new EbhWebServiceConfiguration();
            config.Stub(x => x.RequestTimeout).Return("3000");
            config.Stub(x => x.ProviderKey).Return("wrongKey"); // emulate authorization error in web request
            config.Stub(x => x.Url)
                //.Return("https://ebillinghub.azure-api.net/not-valid/"); // it generates NotFound Http status
                .Return(realConfig.Url); // it should generate Unauthorized Http status because of wrong ProviderKey

            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(config, log);
            //ebhh.Stub(h => h.POSTValidationRequest(null, null, null)).IgnoreArguments().Throw(new WebException() {  });

            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();
            ioh.Stub(h => h.ReadLoadFile(null)).IgnoreArguments().Return("<UBFDocument><producer producerId = \"405\"></producer></UBFDocument>");

            // Act
            var rec = new EbhServiceBusValidationReceiver(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(
                x => x.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Times(7));
            sbh.AssertWasCalled(
                x => x.SendMessage(Arg<IBrokeredMessage>.Is.Anything),
                o => o.Repeat.Once());
            sbh.AssertWasCalled(
                x => x.TryCompleteMsg(Arg<IBrokeredMessage>.Is.Anything),
                o => o.Repeat.Once());
            Assert.IsNotNull(rec.LastError);
            Assert.IsInstanceOfType(rec.LastError, typeof(CriticalStopException));
        }

        [TestMethod]
        [TestCategory("Connected")]
        public void HealthCheckTest()
        {
            // Arrange
            var realConfig = new EbhWebServiceConfiguration();

            // Act
            var svc = new EbhWebService(realConfig, log);
            var running = svc.HealthCheck();

            // Assert
            Assert.IsTrue(running);
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Times(3));
        }

        [TestMethod]
        public void SenderFailNullGetRequestTest()
        {
            // Arrange
            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Properties.Add("username", "user");
            msg.Properties.Add("trackingid", "1111");
            msg.Properties.Add("producerid", "405");
            msg.Stub(h => h.RenewLock()).Return(true);

            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            ebhh.Stub(h => h.TryGetRequest(null, null)).IgnoreArguments().Return(null);// null from TryGetRequest

            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();
            var rec = new EbhServiceBusValidationResultReciever(sbh, ebhh, ioh, log);

            // Act, Assert
            try
            {
                var result = rec.ProcessMessage(msg);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message == "No reponse from Polling");
                ebhh.AssertWasCalled(h => h.GetValidationResultByTrackingId(Arg<string>.Is.Anything, Arg<string>.Is.Anything, Arg<string>.Is.Anything));
            }
            log.AssertWasNotCalled(
                h => h.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Twice());
        }

        [TestMethod]
        public void InvoiceSubmissionReceiverTest()
        {
            // Arrange
            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Stub(h => h.RenewLock()).Return(true);
            msg.Properties.Add("username", "user");
            msg.Properties.Add("loadfilepath", "xyz.xml");
            msg.Properties.Add("producerid", "405");

            var sbh = MockRepository.GenerateStub<IServiceBusHandler>();
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());
            sbh.Stub(x => x.TryCompleteMsg(Arg<IBrokeredMessage>.Is.Anything))
                .Return(true);

            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            ebhh.Stub(h => h.PostInvoiceSubmissionRequest(null, null, null, null)).IgnoreArguments().Return("{    \"Id\": 11,    \"PostTime\": \"2017-10-13T09:44:36.988063Z\"}");

            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>(log);
            ioh.Stub(h => h.ReadLoadFile(null)).IgnoreArguments().Return("<UBFDocument><producer producerId = \"405\"></producer></UBFDocument>");

            // Act
            var rec = new EbhServiceBusInvoiceSubmissionReceiver(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(
                h => h.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            sbh.AssertWasCalled(
                a => a.TryCompleteMsg(Arg<IBrokeredMessage>.Is.Anything),
                o => o.Repeat.Once());
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Times(3));
        }

        [TestMethod]
        public void InvoiceSubmissionReceiverFailNoMsgTest()
        {
            // Arrange
            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();

            // Act
            var rec = new EbhServiceBusInvoiceSubmissionReceiver(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(null);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(
                x => x.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasNotCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything));
        }

        [TestMethod]
        public void InvoiceSubmissionReceiverFailEmptyLoadFileTest()
        {
            // Arrange
            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();
            var rec = new EbhServiceBusInvoiceSubmissionReceiver(sbh, ebhh, ioh, log);

            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Stub(h => h.RenewLock()).Return(true);
            msg.Properties.Add("username", "user");
            msg.Properties.Add("loadfilepath", "xyz.xml");
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            // Empty Load file
            ioh.Stub(h => h.ReadLoadFile(null)).IgnoreArguments().Return("");

            // Act
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(h => h.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Once());
        }

        [TestMethod]
        public void InvoiceSubmissionReceiverFailNoFileTest()
        {
            // Arrange
            var sbh = MockRepository.GenerateMock<IServiceBusHandler>();
            var ebhh = MockRepository.GeneratePartialMock<EbhWebService>(new EbhWebServiceConfiguration(), log);
            var ioh = MockRepository.GeneratePartialMock<NxIOHelper>();

            var msg = MockRepository.GeneratePartialMock<NxBrokeredMessage>();
            msg.Stub(h => h.RenewLock()).Return(true);
            msg.Properties.Add("username", "user");
            msg.Properties.Add("loadfilepath", "xyz.xml");
            sbh.Stub(h => h.TryCompleteMsg(null)).IgnoreArguments().Return(true);
            sbh.Stub(h => h.SendMessage(null)).IgnoreArguments();
            sbh.Stub(h => h.CreateBrokeredMessage()).Return(new NxBrokeredMessage());

            ioh.Stub(h => h.ReadLoadFile(null)).IgnoreArguments().Return("<UBFDocument><producer producerId = \"405\"></producer></UBFDocument>");

            ebhh.Stub(h => h.PostInvoiceSubmissionRequest(null, null, null, null)).IgnoreArguments().Return("");

            // Act
            var rec = new EbhServiceBusInvoiceSubmissionReceiver(sbh, ebhh, ioh, log);
            var result = rec.ProcessMessage(msg);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
            log.AssertWasNotCalled(
                h => h.WriteLog(Arg<string>.Is.Anything, Arg<ILogMgr.SeverityLevel>.Is.Anything, Arg<string[]>.Is.Anything));
            log.AssertWasCalled(
                x => x.Info(Arg<string>.Is.Anything, Arg<string[]>.Is.Anything),
                o => o.Repeat.Twice());
        }
    }
}
