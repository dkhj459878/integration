﻿using NextGen.Framework.Managers.ConfigurationMgr;
using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.StorageMgr;
using System;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace NextGen.Framework.Integration.EBH.Helper
{
    public static class Globals
    {
        const string EBHSBSERVICELOGGING = "EBHSBServiceLogging";
        private static ILogMgr _logMgr;
        
        public static ILogMgr LogMgr
        {
            get { return _logMgr; }
        }

        public static void InitForAzureFunc(IContainerCfgValueProvider cfg)
        {
            StorageMgr.Initialize(cfg, _logMgr);
        }

        public static void InitForAzureLog(ILogMgr logMgr)
        {
            _logMgr = logMgr;
        }

        public static void InitForOnPrem()
        {
            if (_logMgr == null)
                _logMgr = new NxLogMgr();

            if (Convert.ToBoolean(ConfigurationManager.AppSettings[EBHSBSERVICELOGGING]))
            {
                NxTextFileListener listner = new NxTextFileListener() { ImmediateClose = true, AppendToFile = true };
                listner.FileName = "EBHSBService.log";
                listner.Formatter = new NxSimpleFormatter();
                listner.SeverityLevels = ILogMgr.SeverityLevel.All;
                listner.BaseLogPathOrName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                _logMgr.ApplicationListeners.Add(listner);
            }

            if (StorageMgr.Current == null)
                StorageMgr.Initialize(new ConfigValuesProviderCfgFile(NxConfigMgr.Current, _logMgr), _logMgr);
        }

        public static void InitForOnPrem(ILogMgr logMgr)
        {
            _logMgr = logMgr ?? throw new ArgumentNullException(nameof(logMgr));
            InitForOnPrem();
        }
    }
}