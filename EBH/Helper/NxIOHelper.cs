﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.PrintMgr;
using NextGen.Framework.Managers.StorageMgr;
using Storage.Interfaces;
using System;
using System.IO;
using System.Text;

namespace NextGen.Framework.Integration.EBH.Helper
{
    public class NxIOHelper
    {
        private readonly ILogMgr _logger;
        private readonly IStorage _outputStorage;

        protected NxIOHelper()
        {
            var storageMgr = StorageMgr.Current;
            _outputStorage = storageMgr.GetApplicationOutputStorage(ApplicationOutputStorage.Default);
        }

        private ILogMgr InitializeLogger(ILogMgr logMgr)
        {
            return logMgr ?? Globals.LogMgr ?? throw new ArgumentNullException(nameof(logMgr), $"Cannot inject the {nameof(ILogMgr)} object.");
        }

        public NxIOHelper(ILogMgr logMgr) : this()
        {
            _logger = InitializeLogger(logMgr);
        }

        public NxIOHelper(ILogMgr logMgr, IStorage storage)
        {
            _logger = InitializeLogger(logMgr);
            _outputStorage = storage ?? throw new ArgumentNullException(nameof(storage), $"Cannot inject the {nameof(IStorage)} object.");
        }

        public NxIOHelper(ILogMgr logMgr, string storageName)
        {
            _logger = InitializeLogger(logMgr);

            if (string.IsNullOrEmpty(storageName))
                throw new ArgumentNullException(nameof(storageName), $"No storage name specified. Cannot inject the {nameof(IStorage)} object.");

            _outputStorage = StorageMgr.Current.GetStorage(storageName);
        }

        public virtual string ReadLoadFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
                return string.Empty;
            _logger.WriteLog("Reading File", ILogMgr.SeverityLevel.Info);
            string ubf = string.Empty;
            var pi = new PathInfo(filePath);
            try
            {
                if (_outputStorage.FileExists(pi))
                {
                    _logger.WriteLog("File exists", ILogMgr.SeverityLevel.Info);
                    using (var ms = _outputStorage.ReadFile<MemoryStream>(pi))
                    {
                        ms.Position = 0;
                        var streamReader = new StreamReader(ms);
                        ubf = streamReader.ReadToEnd();

                        ms.Close();
                    }
                    _logger.WriteLog("File read complete", ILogMgr.SeverityLevel.Info);
                }
            }
            catch (Exception ex)
            {
                _logger.WriteLog("File read error", ILogMgr.SeverityLevel.Info);
                _logger.Error(ex);
            }
            //todo: delete file
            return ubf;
        }

        public virtual Stream ReadStream(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentNullException(nameof(filePath));

            MemoryStream stream = null;
            _logger.Info($"Reading File. Path: '{filePath}'");
            var file = new PathInfo(filePath);
            try
            {
                if (_outputStorage.FileExists(file))
                {
                    _logger.Info("File exists.");
                    stream = _outputStorage.ReadFile<MemoryStream>(file);
                }
                else
                {
                    _logger.Info($"File '{filePath}' does not exist!");
                }
            }
            catch (Exception ex)
            {
                _logger.Info($"File read error. Path: '{filePath}'");
                _logger.Error(ex);
            }
            return stream;
        }

        public virtual PathInfo WriteOutputFile(string fileName, string flatFile)
        {
			return WriteOutputFile(fileName, flatFile, "EBHResult");
        }

		public virtual PathInfo WriteOutputFile(string fileName, string flatFile, string folderName)
		{
			var ms = new MemoryStream(Encoding.UTF8.GetBytes(flatFile));
			var pi = new PathInfo(folderName, fileName);
			try
			{
				_outputStorage.CreateFolderIfNotExists(folderName);
				if (_outputStorage.FileExists(pi))
                    _logger.Error($"Output file already exists {fileName}");
				else
					_outputStorage.WriteFile(pi, ms);
			}
			catch
			{
                _logger.Error($"Could not write output file {fileName}");
			}
			return pi;
		}

		public IStorage Storage
        {
            get { return _outputStorage; }
        }

    }
}
