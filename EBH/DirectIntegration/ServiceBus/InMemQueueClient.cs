﻿using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Threading.Tasks;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.ServiceBus
{
    public class InMemQueueClient : IQueueClient
    {
        private readonly string _nameSpace;
        private readonly string _queue;
        private readonly MessageRouter _messageRouter;

        public InMemQueueClient(string nameSpace, string queue, MessageRouter messageRouter)
        {
            _nameSpace = nameSpace;
            _queue = queue;
            _messageRouter = messageRouter;
        }


        public void Send(IBrokeredMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var msgToSend = message;
            if (!(msgToSend is InMemBrokeredMessage))
            {
                msgToSend = new InMemBrokeredMessage(message);
            }
            _messageRouter.ProcessSend(_queue, msgToSend);
        }

        public void Close()
        {
           
        }

        #region NotSupported implementations

        public IBrokeredMessage Receive()
        {
            throw new NotSupportedException();
        }

        public IBrokeredMessage Receive(TimeSpan ts)
        {
            throw new NotSupportedException();
        }

        public IBrokeredMessage Receive(long sequenceNumber)
        {
            throw new NotSupportedException();
        }

        public Task<IBrokeredMessage> ReceiveAsync()
        {
            throw new NotSupportedException();
        }

        public Task<IBrokeredMessage> ReceiveAsync(TimeSpan ts)
        {
            throw new NotSupportedException();
        }

        public Task<IBrokeredMessage> ReceiveAsync(long sequenceNumber)
        {
            throw new NotSupportedException();
        }

        public void OnMessage(Action<IBrokeredMessage> callback)
        {
            throw new NotSupportedException();
        }

        public void OnMessage(Action<IBrokeredMessage> callback, int maxConcurrentCalls)
        {
            throw new NotSupportedException();
        }

        public void OnMessageAsync(Func<IBrokeredMessage, Task> callback)
        {
            throw new NotSupportedException();
        }

        public IMessageSession AcceptMessageSession(string sessionID)
        {
            throw new NotSupportedException();
        }

        public IBrokeredMessage CreateJsonMessage(object messageBody)
        {
            throw new NotSupportedException();
        }
        
        #endregion
    }
}