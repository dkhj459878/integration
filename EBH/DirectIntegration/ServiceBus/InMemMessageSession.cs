﻿using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Threading.Tasks;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.ServiceBus
{
    public class InMemMessageSession : IMessageSession
    {
        private readonly string _queueName;
        private readonly string _sessionId;
        private MessageRouter _messageRouter;
        private bool _isClosed = false;
        private bool disposedValue;

        public InMemMessageSession(string queueName, string sessionId, MessageRouter messageRouter)
        {
            _queueName = queueName;
            _sessionId = sessionId;
            _messageRouter = messageRouter;
        }

        public IBrokeredMessage Receive()
        {
            return _messageRouter.ProcessReceive(_queueName, _sessionId);
        }

        public IBrokeredMessage Receive(TimeSpan ts)
        {
            return Receive();
        }

        public void Close()
        {
            _messageRouter.Clean(_queueName, _sessionId);
            _isClosed = true;
        }

        public bool IsClosed => _isClosed;

        #region NotSupported implementations

        public IBrokeredMessage Receive(long sequenceNumber)
        {
            throw new NotSupportedException();
        }

        public Task<IBrokeredMessage> ReceiveAsync()
        {
            throw new NotSupportedException();
        }

        public Task<IBrokeredMessage> ReceiveAsync(TimeSpan ts)
        {
            throw new NotSupportedException();
        }

        public Task<IBrokeredMessage> ReceiveAsync(long sequenceNumber)
        {
            throw new NotSupportedException();
        }

        public void OnMessage(Action<IBrokeredMessage> callback)
        {
            throw new NotSupportedException();
        }

        public void OnMessage(Action<IBrokeredMessage> callback, int maxConcurrentCalls)
        {
            throw new NotSupportedException();
        }

        public void OnMessageAsync(Func<IBrokeredMessage, Task> callback)
        {
            throw new NotSupportedException();
        }

        #endregion

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Close();
                }

                _messageRouter = null;

                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~InMemMessageSession()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}