﻿using NextGen.Framework.Managers.ServiceBusMgr;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.ServiceBus
{
    public class InMemNamespaceQueueItem : INamespaceQueueItem
	{
		public InMemNamespaceQueueItem (string nameSpace, string queueName)
		{
			NamespaceName = nameSpace;
			QueueName = queueName;
		}
		public bool IsSessionful
		{
			get
			{
				return true;
			}
		}

		public string NamespaceName { get; }
		

		public string QueueName { get; }
	}
}
