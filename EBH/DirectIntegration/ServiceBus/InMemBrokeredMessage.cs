﻿using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Generic;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.ServiceBus
{
    public class InMemBrokeredMessage : IBrokeredMessage
    {
        public InMemBrokeredMessage()
        {
        }

        private readonly IDictionary<string, object> extraProperties;

        private const string StatusCode = "statuscode";

        public InMemBrokeredMessage(IBrokeredMessage originalMessage, IDictionary<string, object> extraProps)
        {
            if (originalMessage != null)
                CopyMsgProps(originalMessage);

            if (extraProps == null) return;

            extraProperties = extraProps;
            SetExtraProps();
        }

        private void CopyMsgProps(IBrokeredMessage source)
        {
            CorrelationId = source.CorrelationId;
            Label = source.Label;
            MessageId = source.MessageId;
            SessionId = source.SessionId;
            foreach (var kv in source.Properties)
                Properties.Add(kv.Key, kv.Value);
        }

        private void SetExtraProps()
        {
            if (extraProperties == null)
                return;

            foreach (var property in extraProperties)
            {
                if (!Properties.ContainsKey(property.Key))
                    AddExtraProperty(property);
            }
        }

        private void AddExtraProperty(KeyValuePair<string, object> property)
        {
            var value = property.Key == StatusCode ? property.Value.ToString() : property.Value;

            if (!Properties.ContainsKey(property.Key))
                Properties.Add(property.Key, value);
        }

        public InMemBrokeredMessage(IBrokeredMessage message)
        {
            SafeAssign(() => this.Label = message.Label);
            SafeAssign(() => this.CorrelationId = message.CorrelationId);
            SafeAssign(() => this.SequenceNumber = message.SequenceNumber);
            SafeAssign(() => this.SessionId = message.SessionId);
            SafeAssign(() => this.MessageId = message.MessageId);
            SafeAssign(() => this.TimeToLive = message.TimeToLive);
            SafeAssign(() => this.ReplyTo = message.ReplyTo);

            this.Properties = new Dictionary<string, object>(message.Properties); 
        }

        public void Complete()
        {
            
        }

        public bool RenewLock()
        {
            return true;
        }

        public object ToNativeMessage()
        {
            return this;
        }

        public string Label { get; set; }
        public string CorrelationId { get; set; }
        public long SequenceNumber { get; private set; }
        public string MessageId { get; set; }
        public string SessionId { get; set; }
        public IDictionary<string, object> Properties { get; } = new Dictionary<string, object>();
        public TimeSpan TimeToLive { get; set; }
        public string ReplyTo { get; set; }

        private void SafeAssign(Action act)
        {
            try { act();} catch {}
        }
    }
}