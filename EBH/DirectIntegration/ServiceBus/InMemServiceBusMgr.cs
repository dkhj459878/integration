﻿using NextGen.Framework.Managers.ConfigurationMgr;
using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using System.Collections.Generic;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.ServiceBus
{
    public class InMemServiceBusMgr : IServiceBusMgr
    {
        private readonly MessageRouter _messageRouter;

        
        public InMemServiceBusMgr(MessageRouter messageRouter)
        {
            _messageRouter = messageRouter;
        }

        public IMessageSession GetMessageSession(string nameSpace, string queue, string sessionID)
        {
            return new InMemMessageSession(queue, sessionID, _messageRouter);
        }

        public IQueueClient GetQueueClient(string nameSpace, string queue)
        {
            return _messageRouter.ResolveQueueClient(nameSpace, queue);
        }

        public bool TryCompleteMsg(IQueueClient qc, IBrokeredMessage msg, int retryMax = 1)
        {
            return true;
        }

        public bool TryCompleteMsg(IMessageSession qc, IBrokeredMessage msg, int retryMax = 1)
        {
            return true;
        }

        public INamespaceManager GetNamespace(string nameSpace)
        {
            return new InMemNamespaceManager(nameSpace);
        }

        public INamespaceManager GetNamespace(NamespaceTarget target)
        {
            return new InMemNamespaceManager(target);
        }

		public IReadOnlyList<INamespaceQueueItem> RegisteredQueues => _messageRouter.GetRegisteredQueues();

		public bool IsConnected => true; // For InMem implementation it is always true



        #region NotSupported implementations

        public void PurgeQueue(string nameSpace, string queue)
        {
            throw new System.NotSupportedException();
        }

        public void PurgeQueues()
        {
            throw new System.NotSupportedException();
        }

        public bool RegisterNamespace(string nameSpace, NamespaceTarget target = NamespaceTarget.NONE)
        {
            throw new System.NotSupportedException();
        }

        public bool RegisterNamespaceAzure(string nameSpace, NamespaceTarget target = NamespaceTarget.NONE)
        {
            throw new System.NotSupportedException();
        }

        public bool RegisterQueue(string nameSpace, string queue, bool isSessionful = false)
        {
            throw new System.NotSupportedException();
        }

        public bool RegisterQueueAzure(string nameSpace, string queue, string connectionString, bool isSessionful = false,
            string replyTo = "")
        {
            throw new System.NotSupportedException();
        }

        public void Init(NxConfigSection configSection, ILogMgr logMgr, string tenantId)
        {
        }

        #endregion

    }
}