﻿using NextGen.Framework.Managers.ServiceBusMgr;
using System;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.ServiceBus
{
    public class InMemNamespaceManager : INamespaceManager
    {
        private readonly NamespaceTarget _nameSpaceTarget;
        private readonly string _nameSpaceName;

        public InMemNamespaceManager(string nameSpaceName)
        {
            _nameSpaceName = nameSpaceName;
        }

        public InMemNamespaceManager(NamespaceTarget nameSpaceTarget)
        {
            _nameSpaceTarget = nameSpaceTarget;
            _nameSpaceName = nameSpaceTarget.ToString();
        }


        public string Name => _nameSpaceName;

        public NamespaceTarget Target => _nameSpaceTarget;

        public bool IsAzure { get; set; }


        #region NotSupported

        public bool IsAbleToConnect
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        public void CreateQueue(string queue, TimeSpan lockDuration, int maxDeliveryCount, bool requireSession)
        {
            throw new NotSupportedException();
        }

        public void DeleteQueue(string queue)
        {
            throw new NotSupportedException();
        }

        public bool QueueExists(string path)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}