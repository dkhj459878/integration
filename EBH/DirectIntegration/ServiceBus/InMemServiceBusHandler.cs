﻿using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Generic;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.ServiceBus
{
    public class InMemServiceBusHandler : IServiceBusHandler
    {
        private const int Deftimetolive = 20;
        private readonly string _targetQueueName;
        private readonly MessageRouter _messageRouter;

        public InMemServiceBusHandler(string targetQueueName, MessageRouter messageRouter)
        {
            _targetQueueName = targetQueueName;
            _messageRouter = messageRouter;
        }

        public void SendMessage(IBrokeredMessage message)
        {
            _messageRouter.ProcessSend(_targetQueueName, message);
        }

        public bool TryCompleteMsg(IBrokeredMessage msg)
        {
            return true;
        }

        public IBrokeredMessage CreateBrokeredMessage()
        {
            return new InMemBrokeredMessage();
        }

        public bool IsConnected { get; } = true;

        
        #region NotSupported implementations

        public IQueueClient ReceiveMessage(Action<IBrokeredMessage> messageHandler, int maxConcurrentCalls)
        {
            throw new NotSupportedException("Not supported for InMem implementation");
        }

        public IBrokeredMessage CreateBrokeredMessage(IBrokeredMessage originalMsg, IDictionary<string, object> extraProps = null)
        {
            return new InMemBrokeredMessage(originalMsg, extraProps) { TimeToLive = new TimeSpan(0, Deftimetolive, 0) };
        }

        #endregion

    }
}