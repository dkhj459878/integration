﻿using NextGen.Framework.Integration.EBH.ServiceBusMessenger;
using NextGen.Framework.Managers.ConfigurationMgr;
using System;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.Configuration
{
    public interface IEBHIntegrationConfiguration
    {
        IntegrationModes Mode { get; }
        IEbhWebServiceConfiguration EbhWebServiceConfiguration { get; }
    }

    public class EBHIntegrationConfiguration : IEBHIntegrationConfiguration
    {
        private readonly IConfigMgr _configMgr;

        public EBHIntegrationConfiguration(IConfigMgr configMgr)
        {
            _configMgr = configMgr;
            CreateConfig();
        }

        public IntegrationModes Mode { get; private set; }
        public IEbhWebServiceConfiguration EbhWebServiceConfiguration { get; private set; }

        private void CreateConfig()
        {
            var integrationSection = _configMgr.GetConfigSection("nextGen/other/EBHIntegration");
            var restServiceSection = _configMgr.GetConfigSection("nextGen/other/EBHIntegration/EBHRestService");
            if (integrationSection != null)
            {
                var modeFromConfig = integrationSection.Attributes["mode"];
                IntegrationModes parsedIntegrationMode;
                if (Enum.TryParse(modeFromConfig, true, out parsedIntegrationMode))
                {
                    Mode = parsedIntegrationMode;
                    EbhWebServiceConfiguration = new InMemEBHWebServiceConfiguration(restServiceSection);
                }
                else
                {
                    Mode = IntegrationModes.ServiceBus;
                }
            }
            else
            {
                Mode = IntegrationModes.ServiceBus;
            }
        }
    }

    public enum IntegrationModes
    {
        Direct,
        ServiceBus
    }
}