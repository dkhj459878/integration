﻿using NextGen.Framework.Integration.EBH.ServiceBusMessenger;
using NextGen.Framework.Managers.ConfigurationMgr;
using System.Linq;

namespace NextGen.Framework.Integration.EBH.DirectIntegration.Configuration
{
    public class InMemEBHWebServiceConfiguration : IEbhWebServiceConfiguration
    {
        public const string UrlSettingName = "url";
        public const string ProviderKeySettingName = "providerkey";
        public const string RequestTimeoutSettingName = "timeout";

        public InMemEBHWebServiceConfiguration(NxConfigSection configSection)
        {
            if (configSection.Attributes.AllKeys.Contains(UrlSettingName))
            {
                Url = configSection.Attributes[UrlSettingName];
            }

            if (configSection.Attributes.AllKeys.Contains(ProviderKeySettingName))
            {
                ProviderKey = configSection.Attributes[ProviderKeySettingName];
            }

            if (configSection.Attributes.AllKeys.Contains(RequestTimeoutSettingName))
            {
                RequestTimeout = configSection.Attributes[RequestTimeoutSettingName];
            }
            else
            {
                RequestTimeout = "40000";
            }
        }

        public string Url { get; }
        public string ProviderKey { get; }
        public string RequestTimeout { get; }
    }
}