﻿using NextGen.Framework.Managers.LogMgr;
using NextGen.Framework.Managers.ServiceBusMgr;
using NextGen.Framework.Managers.StorageMgr;
using System;

namespace NextGen.Framework.Integration.EBH.DirectIntegration
{
    using Configuration;
    using Helper;
    using ServiceBus;
    using ServiceBusMessenger;
    using ServiceBusMessenger.Messengers;
    using ServiceBusMessenger.Queues;

    public class EBHDirectIntegrationFactory
    {
        public static IServiceBusMgr CreateInMemServiceBusMgr(IEBHIntegrationConfiguration configuration, ILogMgr log)
        {
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));

            var webService = new EbhWebService(configuration.EbhWebServiceConfiguration, log);
            var messageRouter = new MessageRouter();
			var ioHelper = new NxIOHelper(log);

			messageRouter.RegisterMessageHandler(
                QueueNames.EbhValidateRequest,
                (message) => new EbhServiceBusValidationReceiver(new InMemServiceBusHandler(QueueNames.EbhValidatePending, messageRouter), webService, ioHelper, log)
                    .ProcessMessage(message)
            );
            messageRouter.RegisterMessageHandler(
                QueueNames.EbhValidatePending,
                (message) => new EbhServiceBusValidationResultReciever(new InMemServiceBusHandler(QueueNames.EbhValidateResponse, messageRouter), webService, ioHelper, log)
                    .ProcessMessage(message)
            );
            messageRouter.RegisterMessageHandler(
                QueueNames.EbhInvoiceSubmissionRequest,
                (message) => new EbhServiceBusInvoiceSubmissionReceiver(new InMemServiceBusHandler(QueueNames.EbhInvoiceSubmissionResponse, messageRouter), webService, ioHelper, log)
                    .ProcessMessage(message)
            );
            messageRouter.RegisterMessageHandler(
                QueueNames.EbhInvoiceStatusRequest,
                (message) => new EbhServiceBusInvoiceStatusReceiver(new InMemServiceBusHandler(QueueNames.EbhInvoiceStatusResponse, messageRouter), webService, ioHelper, log)
                    .ProcessMessage(message)
            );
            messageRouter.RegisterMessageHandler(
                QueueNames.EbhConfigurationRequest,
                (message) => new EbhServiceBusConfigurationReceiver(new InMemServiceBusHandler(QueueNames.EbhConfigurationResponse, messageRouter), webService, ioHelper, log)
                    .ProcessMessage(message)
            );
            var attachmentsIOHelper = new NxIOHelper(log, StorageName.Attachments);
            messageRouter.RegisterMessageHandler(
                QueueNames.EbhAttachmentRequest,
                (message) => new EbhServiceBusAttachmentReceiver(new InMemServiceBusHandler(QueueNames.EbhAttachmentResponse, messageRouter), webService, attachmentsIOHelper, log)
                    .ProcessMessage(message)
            );

            return new InMemServiceBusMgr(messageRouter);
        }
    }
}