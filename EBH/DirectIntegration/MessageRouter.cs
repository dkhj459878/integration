﻿using NextGen.Framework.Managers.ServiceBusMgr;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace NextGen.Framework.Integration.EBH.DirectIntegration
{
    using ServiceBus;
    using ServiceBusMessenger.Queues;

    public class MessageRouter
    {
        protected readonly ConcurrentDictionary<string, IQueueClient> queueClients = new ConcurrentDictionary<string, IQueueClient>();
        protected readonly ConcurrentDictionary<string, Action<IBrokeredMessage>> requestMessageHandlers = new ConcurrentDictionary<string, Action<IBrokeredMessage>>();
        protected readonly ConcurrentDictionary<string, ConcurrentDictionary<string, IBrokeredMessage>> responseMessages 
            = new ConcurrentDictionary<string, ConcurrentDictionary<string, IBrokeredMessage>>();

		internal IReadOnlyList<INamespaceQueueItem> GetRegisteredQueues()
		{
			var responseQueues = responseMessages.Keys.Select(k => new InMemNamespaceQueueItem(QueueNames.Namespace, k));
			var requestQueuest = requestMessageHandlers.Keys.Select(k => new InMemNamespaceQueueItem(QueueNames.Namespace, k));

			return responseQueues.Concat(requestQueuest).ToList();
		}

		public MessageRouter()
        {
            responseMessages[QueueNames.EbhValidateResponse] = new ConcurrentDictionary<string, IBrokeredMessage>();
            responseMessages[QueueNames.EbhConfigurationResponse] = new ConcurrentDictionary<string, IBrokeredMessage>();
            responseMessages[QueueNames.EbhInvoiceStatusResponse] = new ConcurrentDictionary<string, IBrokeredMessage>();
            responseMessages[QueueNames.EbhInvoiceSubmissionResponse] = new ConcurrentDictionary<string, IBrokeredMessage>();
            responseMessages[QueueNames.EbhAttachmentResponse] = new ConcurrentDictionary<string, IBrokeredMessage>();
        }

        public IQueueClient ResolveQueueClient(string nameSpace, string queue)
        {
			queue = ExtractQueueName(queue);
			if (!queueClients.ContainsKey(queue))
                queueClients[queue] = new InMemQueueClient(nameSpace, queue, this);

            return queueClients[queue];
        }

        public void RegisterMessageHandler(string sourceQueueName, Action<IBrokeredMessage> handler)
        {
            requestMessageHandlers[sourceQueueName] = handler;
        }

        public void ProcessSend(string queueName, IBrokeredMessage message)
        {
			queueName = ExtractQueueName(queueName);
			if (requestMessageHandlers.ContainsKey(queueName))
            {
                var handler = requestMessageHandlers[queueName];
                handler?.Invoke(message);
            }
            else if (responseMessages.ContainsKey(queueName))
            {
                var queue = responseMessages[queueName];
                queue.AddOrUpdate(message.SessionId, message, (key, msg) => message);
            }
            else
            {
                throw new NotSupportedException(queueName);
            }
        }

        public IBrokeredMessage ProcessReceive(string queueName, string sessionId)
        {
			queueName = ExtractQueueName(queueName);
			if (!responseMessages.ContainsKey(queueName))
            {
                throw new NotSupportedException(queueName);
            }

            var queue = responseMessages[queueName];
            if (!queue.ContainsKey(sessionId))
            {
                return null;
            }

            queue.TryRemove(sessionId, out IBrokeredMessage message);
            return message;
        }

        public void Clean(string queueName, string sessionId)
        {
			queueName = ExtractQueueName(queueName);
			// prevents potential memory leak.
			IBrokeredMessage removed;
            if (responseMessages.ContainsKey(queueName))
            {
                responseMessages[queueName].TryRemove(sessionId, out removed);
            }
        }

		private string ExtractQueueName(string queueName)
		{
			const char separator = '-';
			if (!queueName.Contains(separator))
			{
				return queueName;
			}

			return queueName.Split(separator)[0];
		}
	}
}