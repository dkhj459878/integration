﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Configuration.Interfaces;
using Configuration.Sources;
using Microsoft.Extensions.Logging;

namespace Configuration
{
    public sealed class ConfigurationSourceFactory : IConfigurationSourceFactory
    {
        private readonly ILogger logger;
        private IConfigurationCacheFactory cacheFactory;
        private ITenantClientFactory tenantClientFactory;

        public ConfigurationSourceFactory(ILogger logger)
        {
            this.logger = logger;
        }

        internal ConfigurationSourceFactory(IConfigurationCacheFactory cacheFactory, ITenantClientFactory tenantClientFactory, ILogger logger)
        {
            this.logger = logger;
            this.cacheFactory = cacheFactory;
            this.tenantClientFactory = tenantClientFactory;
        }

        internal async Task<IConfigurationCacheFactory> GetCacheFactory(IConfigurationSource source)
        {
            if (cacheFactory != null) return cacheFactory;

            var cacheDisabledString = await source.GetSettingAsync(Constants.AppSettings.IsCacheDisabled).ConfigureAwait(false);
            bool.TryParse(cacheDisabledString?.Value, out var isCacheDisabled);
            logger.LogTrace("Use configuration caching? {0}", !isCacheDisabled);
            cacheFactory = new ConfigurationCacheFactory(!isCacheDisabled, logger);
            return cacheFactory;
        }

        public async Task<IConfigurationSource> Create()
        {
            var source = CreateAppSettings();
            var external = await CreateExternalSettings(source).ConfigureAwait(false);
            return new ConfigurationChain(new List<IConfigurationSource> { source, external });
        }

        internal ITenantClientFactory GetTenantFactory(IConfigurationSource source)
        {
            if (tenantClientFactory != null) return tenantClientFactory;
            tenantClientFactory = new TenantClientFactory(source, logger);
            return tenantClientFactory;
        }

        internal async Task<IConfigurationSource> CreateExternalSettings(IConfigurationSource source)
        {
            if (!await ShouldUseRemoteConfiguration(source).ConfigureAwait(false))
            {
                logger.LogInformation("External configuration source is disabled");
                return null;
            }
            logger.LogInformation("External configuration source is enabled");

            var tenantId = (await source.GetSettingAsync(Constants.AppSettings.TRSTenantId).ConfigureAwait(false))?.Value;
            if (string.IsNullOrEmpty(tenantId))
            {
                var ex = new ArgumentException($"Unable to read {Constants.AppSettings.TRSTenantId} setting which is required to fetch external configuration.");
                logger.LogError(ex, ex.Message);
                throw ex;
            }

            var cachingFactory = await GetCacheFactory(source).ConfigureAwait(false);
            var tenantClientFactory = GetTenantFactory(source);
            var client = await tenantClientFactory.Create().ConfigureAwait(false);

            IConfigurationSource external = new ExternalConfigurationSource(tenantId, client, logger);
            TimeSpan cachePeriod = await GetCacheExpiration(source).ConfigureAwait(false);
            return cachingFactory.AddCaching(external, cachePeriod);
        }

        internal IConfigurationSource CreateAppSettings()
        {
            return new EnvironmentConfigurationSource();
        }

        private async Task<bool> ShouldUseRemoteConfiguration(IConfigurationSource source)
        {
            var shouldUseRemoteConfigurationSetting = await source.GetSettingAsync(Constants.AppSettings.TRSIsEnabled).ConfigureAwait(false);

            bool.TryParse(shouldUseRemoteConfigurationSetting?.Value, out var result);

            return result;
        }

        private async Task<TimeSpan> GetCacheExpiration(IConfigurationSource source)
        {
            var cacheExpirationTime = await source.GetSettingAsync(Constants.AppSettings.CacheRefreshPeriod).ConfigureAwait(false);
            const double DEFAULT_PERIOD = 60;
            
            return TimeSpan.FromMinutes(double.TryParse(cacheExpirationTime?.Value, out var result) ? result : DEFAULT_PERIOD);
        }

    }
}
