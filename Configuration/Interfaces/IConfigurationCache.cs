﻿using System;

namespace Configuration.Interfaces
{
    public interface IConfigurationCache
    {
        ConfigurationValue GetSetting(string name);

        void PutSetting(ConfigurationValue value, TimeSpan interval);
    }
}
