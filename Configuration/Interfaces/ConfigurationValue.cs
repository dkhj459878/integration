﻿using System;

namespace Configuration.Interfaces
{
    public class ConfigurationValue
    {
        public ConfigurationValue(string name, string value, DateTimeOffset? expires = null)
        {
            Name = name;
            Value = value;
            Expires = expires;
        }

        public string Name { get; }

        public string Value { get; }

        public DateTimeOffset? Expires { get; }

        public bool IsExpired
        {
            get
            {
                if (Expires.HasValue && Expires.Value <= DateTime.UtcNow)
                    return true;

                return false;
            }
        }

        public override string ToString()
        {
            return string.Format("Name :{0} Value:{1} Expire: {2}", Name, Value, Expires);
        }
    }
}
