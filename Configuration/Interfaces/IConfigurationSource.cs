﻿using System.Threading.Tasks;

namespace Configuration.Interfaces
{
    public interface IConfigurationSource
    {
        /// <summary>
        /// Returns valid configuration value or null (async).
        /// </summary>
        /// <param name="name">Setting name</param>
        /// <returns></returns>
        Task<ConfigurationValue> GetSettingAsync(string name);
    }
}