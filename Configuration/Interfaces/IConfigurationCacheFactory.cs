﻿using System;

namespace Configuration.Interfaces
{
    public interface IConfigurationCacheFactory
    {
        IConfigurationCache CreateCache();
        IConfigurationSource AddCaching(IConfigurationSource current, TimeSpan cachePeriod);
    }
}