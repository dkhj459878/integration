﻿using System.Threading.Tasks;
using Tenant.Client;

namespace Configuration.Interfaces
{
    public interface ITenantClientFactory
    {
        Task<ITenantClient> Create();
    }
}