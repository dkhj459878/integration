﻿using System.Threading.Tasks;

namespace Configuration.Interfaces
{
    public interface IConfigurationSourceFactory
    {
        Task<IConfigurationSource> Create();
    }
}