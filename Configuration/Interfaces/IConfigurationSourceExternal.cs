﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Configuration.Interfaces
{
    public interface IConfigurationSourceExternal
    {
        Task<IEnumerable<IConfigurationSource>> Create(IConfigurationSource source, ILogger logger);
    }
}