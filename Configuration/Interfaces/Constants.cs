﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Configuration.Interfaces
{
    public static class Constants
    {
        public static class AppSettings
        {
            public static IEnumerable<string> GetAll()
            {
                var This = typeof(AppSettings);
                return This.GetFields(BindingFlags.Static | BindingFlags.Public)
                    .Where(f => f.FieldType == typeof(string))
                    .Select(f => f.GetValue(This).ToString());
            }

            public static readonly string ApplicationOutputBlobConnectionStringFromTRS = "applicationoutputBlobConnectionString";
                                                                              
            public static readonly string APPLICATIONOUTPUTCONTAINERNAME = "applicatonOutputContainerName";
            public static readonly string APPLICATIONOUTPUTCONNECTIONSTRING = "applicatonOutputConnectionString";

            public static readonly string AttachmentsBlobConnectionStringFromTRS = "attachmentBlobConnectionString";

            public static readonly string ATTACHMENTCONTAINERNAME = "attachmentContainerName";
            public static readonly string ATTACHMENTCONNECTIONSTRING = "attachmentConnectionString";

            public static readonly string IsCacheDisabled = "IsCacheDisabled";
            public static readonly string CacheRefreshPeriod = "CacheRefreshPeriod";

            public static readonly string TRSTenantId = "H3E-TRSInstanceID";
            public static readonly string TRSIsEnabled = "TRS-IsEnabled";
            public static readonly string TRSAADAppID = "TRS-AADAppID";
            public static readonly string TRSAADAppSecretKey = "TRS-AADAppSecretKey";
            public static readonly string TRSAADServiceAppID = "TRS-AADServiceAppID";
            public static readonly string TRSServiceURI = "TRS-ServiceURI";
            public static readonly string TRSInternalAppRegistrationAAD = "TRS-InternalAppRegistrationAAD";
        }
    }
}