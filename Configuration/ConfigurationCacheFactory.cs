﻿using System;
using Configuration.Cache;
using Configuration.Interfaces;
using Elite3E.Configuration.Cache;
using Microsoft.Extensions.Logging;

namespace Configuration
{
    public class ConfigurationCacheFactory : IConfigurationCacheFactory
    {
        private readonly bool enabled;
        private readonly ILogger logger;

        public ConfigurationCacheFactory(bool enabled, ILogger logger)
        {
            this.enabled = enabled;
            this.logger = logger;
        }

        public IConfigurationSource AddCaching(IConfigurationSource current, TimeSpan cachePeriod)
        {
            if (current == null || !enabled) return current;
            return new CachingConfigurationSource(current, CreateCache(), logger, cachePeriod);
        }

        public IConfigurationCache CreateCache()
        {
            try
            {
                return new ConfigurationCache();
            }
            catch (Exception ex)
            {
                logger.LogTrace(ex, "Failed to create cache container based on System.Runtime.Cache. Will fallback to in-memory cache.");
                return new InMemoryCache();
            }
        }
    }
}
