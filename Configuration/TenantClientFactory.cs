﻿using System;
using System.Threading.Tasks;
using Configuration.Interfaces;
using Microsoft.Extensions.Logging;
using Tenant.Client;

namespace Configuration
{
    public class TenantClientFactory : ITenantClientFactory
    {
        private readonly IConfigurationSource configurationSource;
        private readonly ILogger logger;

        public TenantClientFactory(IConfigurationSource configurationSource, ILogger logger)
        {
            this.configurationSource = configurationSource;
            this.logger = logger;
        }

        public async Task<ITenantClient> Create()
        {
            try
            {
                var clientId = await configurationSource.GetSettingAsync(Constants.AppSettings.TRSAADAppID).ConfigureAwait(false);
                var clientSecret = await configurationSource.GetSettingAsync(Constants.AppSettings.TRSAADAppSecretKey).ConfigureAwait(false);
                var resourceId = await configurationSource.GetSettingAsync(Constants.AppSettings.TRSAADServiceAppID).ConfigureAwait(false);
                var endpoint = await GetEndpoint(configurationSource).ConfigureAwait(false);
                var authority = await configurationSource.GetSettingAsync(Constants.AppSettings.TRSInternalAppRegistrationAAD).ConfigureAwait(false);

                var options = new TenantClientOptions
                {
                    Endpoint = endpoint,
                    ProductName = "H3E",
                    ResourceId = resourceId?.Value,
                    Auth =
                    {
                        Authority = authority?.Value,
                        ClientId = clientId?.Value,
                        ClientSecret = clientSecret?.Value
                    }
                };
                var tenantClientFactory = new WebApiClientFactory();
                return tenantClientFactory.CreateTenantClient(options);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to initialize TRS settings configuration source");
                throw;
            }
        }

        private async Task<string> GetEndpoint(IConfigurationSource source)
        {
            var endpoint = await source.GetSettingAsync(Constants.AppSettings.TRSServiceURI).ConfigureAwait(false);
            return endpoint == null || endpoint.Value == null ? null : endpoint.Value.TrimEnd('/') + "/";
        }
    }
}
