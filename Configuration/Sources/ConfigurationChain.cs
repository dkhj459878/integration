﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Configuration.Interfaces;

namespace Configuration.Sources
{
    public class ConfigurationChain : IConfigurationSource
    {
        public List<IConfigurationSource> Sources { get; }

        public ConfigurationChain(IEnumerable<IConfigurationSource> sources)
        {
            Sources = sources?.Where(item => item != null).ToList() ?? throw new ArgumentNullException(nameof(sources));
        }

        public async Task<ConfigurationValue> GetSettingAsync(string name)
        {
            foreach (var source in Sources)
            {
                var value = await source.GetSettingAsync(name).ConfigureAwait(false);
                if (value != null) return value;
            }
            return null;
        }
    }
}
