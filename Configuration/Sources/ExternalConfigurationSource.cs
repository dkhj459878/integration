﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Configuration.Interfaces;
using Microsoft.Extensions.Logging;
using Tenant.Client;

namespace Configuration.Sources
{
    public sealed class ExternalConfigurationSource : IConfigurationSource
    {
        private const int RetryCount = 2;

        private readonly ITenantClient tenantClient;
        private readonly ILogger logger;
        private readonly string tenantId;

        public ExternalConfigurationSource(string tenantId, ITenantClient tenantClient, ILogger logger)
        {
            this.tenantId = tenantId ?? throw new ArgumentNullException(nameof(tenantId));
            this.tenantClient = tenantClient ?? throw new ArgumentNullException(nameof(tenantClient));
            this.logger = logger;
        }

        public async Task<ConfigurationValue> GetSettingAsync(string name)
        {
            logger.LogTrace($"Loading external settings. Machine: {Environment.MachineName} Process: {Process.GetCurrentProcess().Id} Domain: {AppDomain.CurrentDomain.Id}");
            logger.LogTrace($"Loading external setting '{name}'");

            var retryCount = RetryCount;

            while (retryCount > 0)
            {
                retryCount--;

                try
                {
                    var configProvider = await tenantClient.GetConfigAsync(tenantId, "H3E", name).ConfigureAwait(false);
                    if (configProvider == null)
                    {
                        throw new Exception("Tenant.Client returned an empty configuration provider");
                    }

                    var setting = configProvider.Get(name);
                    if (setting == null || setting.IsMissing)
                    {
                        logger.LogWarning($"Setting '{name}' doesn't exist or has null value");
                        return null;
                    }

                    logger.LogTrace($"Successfully retrieved setting '{name}'");
                    return new ConfigurationValue(setting.Name, setting.Value, setting.Expires);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Error loading external configuration.");

                    if (retryCount == 0)
                        throw;
                }

                logger.LogWarning("Retrieval failed, {0} attempts left.", retryCount);
                await Task.Delay(1000).ConfigureAwait(false);
            }

            throw new InvalidOperationException($"Failed to retrieve configuration setting {name} in {RetryCount} attempts.");
        }
    }
}
