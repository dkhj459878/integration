﻿using System.IO;
using System.Threading.Tasks;
using Configuration.Interfaces;
using Microsoft.Extensions.Configuration;
using IConfigurationSource = Configuration.Interfaces.IConfigurationSource;

namespace Configuration.Sources
{
    public sealed class EnvironmentConfigurationSource : IConfigurationSource
    {
        private IConfigurationRoot config;

        public Task<ConfigurationValue> GetSettingAsync(string name)
        {
            return Task.FromResult(GetSetting(name));
        }

        internal IConfigurationRoot ConfigurationRoot
        {
            get
            {
                if (config == null)
                {
                    config = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .Build();
                }

                return config;
            }
            set
            {
                config = value;
            }
        }

        private ConfigurationValue GetSetting(string name)
        {
            var value = System.Environment.GetEnvironmentVariable(name) ?? ConfigurationRoot[name];

            return value == null ? null : new ConfigurationValue(name, value);
        }
    }
}