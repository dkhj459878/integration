﻿using System;
using System.Threading.Tasks;
using Configuration.Interfaces;
using Microsoft.Extensions.Logging;

namespace Configuration.Cache
{
    internal sealed class CachingConfigurationSource : IConfigurationSource
    {
        private TimeSpan cachingInterval;
        private IConfigurationSource configurationSource;
        private IConfigurationCache cache;
        private ILogger logger;

        public CachingConfigurationSource(IConfigurationSource configurationSource, IConfigurationCache cache, ILogger logger, TimeSpan cachingInterval)
        {
            this.configurationSource = configurationSource ?? throw new ArgumentNullException(nameof(configurationSource));
            this.cache = cache;
            this.logger = logger;

            this.cachingInterval = cachingInterval;
        }

        public async Task<ConfigurationValue> GetSettingAsync(string name)
        {
            var value = cache.GetSetting(name);

            if (value == null)
            {
                logger.LogTrace($"Value not found in cache: '{name}' Interval: {cachingInterval} Now: {DateTime.UtcNow}");
                value = await configurationSource.GetSettingAsync(name).ConfigureAwait(false);
                if (value != null)
                {
                    logger.LogTrace($"Setting '{name}' was loaded from inner configuration source and will be cached.");
                    cache.PutSetting(value, cachingInterval);
                    return value;
                }
                logger.LogTrace($"Setting '{name}' was not found in inner configuration source.");
            }

            return value;
        }
        
    }
}