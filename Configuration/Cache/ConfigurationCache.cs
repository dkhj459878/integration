﻿using System;
using System.Runtime.Caching;
using Configuration.Interfaces;

namespace Elite3E.Configuration.Cache
{
    internal sealed class ConfigurationCache : IConfigurationCache
    {
        private static MemoryCache memoryCache;

        public ConfigurationCache(string name = "Function") : this(new MemoryCache(name)) //will throw if platform doesn't support memory cache
        { }

        internal ConfigurationCache(MemoryCache memoryCache)
        {
            ConfigurationCache.memoryCache = memoryCache;
        }

        public ConfigurationValue GetSetting(string name)
        {
            return (ConfigurationValue)memoryCache.Get(name);
        }

        public void PutSetting(ConfigurationValue value, TimeSpan interval)
        {
            CacheItem item = new CacheItem(value.Name, value);

            var expires = value?.Expires;

            DateTimeOffset cachingInterval = GetCachingInterval(interval, expires);

            memoryCache.Set(item, new CacheItemPolicy()
            {
                AbsoluteExpiration = DateTimeOffset.UtcNow.Add(cachingInterval - DateTimeOffset.UtcNow)
            });
        }

        internal static DateTimeOffset GetCachingInterval(TimeSpan interval, DateTimeOffset? expires)
        {
            return expires.HasValue && expires.Value < DateTimeOffset.UtcNow.Add(interval)
                ? expires.Value : DateTimeOffset.UtcNow.Add(interval);
        }
    }
}
