﻿using System;
using System.Collections.Generic;
using Configuration.Cache;
using Configuration.Interfaces;

namespace Elite3E.Configuration.Cache
{
    internal sealed class InMemoryCache : IConfigurationCache
    {
        private static readonly Dictionary<string, InMemoryCacheItem> cache = new Dictionary<string, InMemoryCacheItem>();

        public ConfigurationValue GetSetting(string name)
        {
            if (cache.TryGetValue(name, out var cachedValue) && cachedValue?.Expires > DateTimeOffset.UtcNow)
            {
                return cachedValue?.ConfigurationValue;
            }

            return null;
        }

        public void PutSetting(ConfigurationValue value, TimeSpan interval)
        {
            var expires = value?.Expires;

            DateTimeOffset cachingInterval = GetCachingInterval(interval, expires);

            cache[value.Name] = new InMemoryCacheItem(value, cachingInterval);
        }

        internal static DateTimeOffset GetCachingInterval(TimeSpan interval, DateTimeOffset? expires)
        {
            return expires.HasValue && expires.Value < DateTimeOffset.UtcNow.Add(interval)
                ? expires.Value : DateTimeOffset.UtcNow.Add(interval);
        }
    }
}