﻿using System;
using Configuration.Interfaces;

namespace Configuration.Cache
{
    public class InMemoryCacheItem
    {
        public InMemoryCacheItem(ConfigurationValue configurationValue, DateTimeOffset? expires = null)
        {
            ConfigurationValue = configurationValue;
            Expires = expires;
        }

        public ConfigurationValue ConfigurationValue { get; }

        public DateTimeOffset? Expires { get; }
    }
}
